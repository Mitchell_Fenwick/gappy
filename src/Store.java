import java.util.*;

/**
 * @version 1.0
 * @author C-M-Bizzle
 *
 */
public class Store {
	private static ArrayList<Food> food = new ArrayList<Food>();
	private static ArrayList<Toy> toys = new ArrayList<Toy>();
	
	public static void init() {
		food.add(new Steak());
		food.add(new TinnedKangaroo());
		food.add(new Sausage());
		food.add(new Donut());
		food.add(new DeadMouse());
		food.add(new Biscuit());
		toys.add(new Stick());
		toys.add(new Ball());
		toys.add(new BallOfString());
		toys.add(new Bone());
		toys.add(new Tunnel());
		toys.add(new StuffedBear());
	}
	
	/**
	 * returns food
	 * @return
	 */
	public static ArrayList<Food> getFood() { 
		return food;
	}
	
	/**
	 * return toys
	 * @return
	 */
	public static ArrayList<Toy> getToys() {
		return toys;
	}
	
	/**
	 * adds toy to players toy array
	 * @param player
	 * @param toy
	 */
	public static void buyToy(Player player, Toy toy){
		player.addToy(toy);
		player.changeMoney(toy.getPrice()*-1);
	}
	
	/**
	 * adds food to player food array
	 * @param player
	 * @param food
	 */
	public static void buyFood(Player player, Food food) {
		player.addFood(food);
		player.changeMoney(food.getPrice()*-1);
	}
}

