import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class DeathBox extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DeathBox dialog = new DeathBox();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DeathBox() {
		setBounds(100, 100, 260, 150);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
			
		JLabel pet1Lbl = new JLabel(GameEnv.getPetName().getName());
		GridBagConstraints gbc_pet1Lbl = new GridBagConstraints();
		gbc_pet1Lbl.gridwidth = 2;
		gbc_pet1Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_pet1Lbl.gridx = 0;
		gbc_pet1Lbl.gridy = 0;
		contentPanel.add(pet1Lbl, gbc_pet1Lbl);
		
		JLabel oneLbl = new JLabel("Your pet died");
		GridBagConstraints gbc_oneLbl = new GridBagConstraints();
		gbc_oneLbl.gridwidth = 2;
		gbc_oneLbl.insets = new Insets(0, 0, 5, 0);
		gbc_oneLbl.gridx = 0;
		gbc_oneLbl.gridy = 1;
		contentPanel.add(oneLbl, gbc_oneLbl);
			
		JLabel revivesLbl = new JLabel("Hah! you have no more revives");
		revivesLbl.setVisible(false);
		GridBagConstraints gbc_revivesLbl = new GridBagConstraints();
		gbc_revivesLbl.gridwidth = 2;
		gbc_revivesLbl.insets = new Insets(0, 0, 5, 5);
		gbc_revivesLbl.gridx = 0;
		gbc_revivesLbl.gridy = 2;
		contentPanel.add(revivesLbl, gbc_revivesLbl);
		
		JButton cure1Btn_1 = new JButton("Revive");
		cure1Btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!GameEnv.getPetName().getDeath()) {
					GameEnv.getPetName().revive();
					dispose();
				}else{
					revivesLbl.setVisible(true);
				}
			}
		});
		GridBagConstraints gbc_cure1Btn_1 = new GridBagConstraints();
		gbc_cure1Btn_1.insets = new Insets(0, 0, 0, 5);
		gbc_cure1Btn_1.gridx = 0;
		gbc_cure1Btn_1.gridy = 3;
		contentPanel.add(cure1Btn_1, gbc_cure1Btn_1);
		cure1Btn_1.setActionCommand("OK");
		getRootPane().setDefaultButton(cure1Btn_1);
			
		JButton ignore1Btn_1 = new JButton("kill");
		ignore1Btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GameEnv.getPetName().misbehaved();
				GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getPets().remove(GameEnv.getPetName());
				dispose();
			}
		});
		GridBagConstraints gbc_ignore1Btn_1 = new GridBagConstraints();
		gbc_ignore1Btn_1.gridx = 1;
		gbc_ignore1Btn_1.gridy = 3;
		contentPanel.add(ignore1Btn_1, gbc_ignore1Btn_1);
		ignore1Btn_1.setActionCommand("Cancel");
	}

}
