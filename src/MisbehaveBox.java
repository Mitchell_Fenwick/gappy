import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MisbehaveBox extends JDialog {

	private final JPanel contentPanel = new JPanel();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MisbehaveBox dialog = new MisbehaveBox();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MisbehaveBox() {
		setBounds(100, 100, 260, 150);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
			
		JLabel pet1Lbl = new JLabel(GameEnv.getPetName().getName());
		GridBagConstraints gbc_pet1Lbl = new GridBagConstraints();
		gbc_pet1Lbl.gridwidth = 2;
		gbc_pet1Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_pet1Lbl.gridx = 0;
		gbc_pet1Lbl.gridy = 0;
		contentPanel.add(pet1Lbl, gbc_pet1Lbl);
		
		JLabel oneLbl = new JLabel("Your pet misbehaved");
		GridBagConstraints gbc_oneLbl = new GridBagConstraints();
		gbc_oneLbl.gridwidth = 2;
		gbc_oneLbl.insets = new Insets(0, 0, 5, 0);
		gbc_oneLbl.gridx = 0;
		gbc_oneLbl.gridy = 1;
		contentPanel.add(oneLbl, gbc_oneLbl);
			
		JButton cure1Btn_1 = new JButton("punish");
		cure1Btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GameEnv.getPetName().punish();
				dispose();
			}
		});
		GridBagConstraints gbc_cure1Btn_1 = new GridBagConstraints();
		gbc_cure1Btn_1.insets = new Insets(0, 0, 0, 5);
		gbc_cure1Btn_1.gridx = 0;
		gbc_cure1Btn_1.gridy = 2;
		contentPanel.add(cure1Btn_1, gbc_cure1Btn_1);
		cure1Btn_1.setActionCommand("OK");
		getRootPane().setDefaultButton(cure1Btn_1);
			
		JButton ignore1Btn_1 = new JButton("ignore");
		ignore1Btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GameEnv.getPetName().misbehaved();
				dispose();
			}
		});
		GridBagConstraints gbc_ignore1Btn_1 = new GridBagConstraints();
		gbc_ignore1Btn_1.gridx = 1;
		gbc_ignore1Btn_1.gridy = 2;
		contentPanel.add(ignore1Btn_1, gbc_ignore1Btn_1);
		ignore1Btn_1.setActionCommand("Cancel");
	}


}
