
public class Food {
	private int nutrition;
	private int tasteValue;
	private double price;
	private double weight;
	private int toiletValue;
	
	/**
	 * constructor
	 * @param nutrition
	 * @param tasteValue
	 * @param price
	 * @param weight
	 */
	public Food(int nutrition, int tasteValue, double price, double weight, int toiletValue) {
		this.nutrition = nutrition;
		this.tasteValue = tasteValue;
		this.price = price;
		this.weight = weight;
		this.toiletValue = toiletValue;
	}
	
	public double getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	public double getWeight() {
		return weight;
	}
	public int getTasteValue() {
		return tasteValue;
	}
	public int getNutrition() {
		return nutrition;
	}
	
	public int getToiletValue() {
		return toiletValue;

	}
	
}
