import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

public class PlayBox extends JDialog {
	private int pet = GameEnv.getCurrentPet(); 
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PlayBox dialog = new PlayBox();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PlayBox() {
		setBounds(100, 100, 300 , 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
	
		JTextPane toysTP = new JTextPane();
		GridBagConstraints gbc_toysTP = new GridBagConstraints();
		gbc_toysTP.gridheight = 5;
		gbc_toysTP.insets = new Insets(0, 0, 5, 0);
		gbc_toysTP.fill = GridBagConstraints.BOTH;
		gbc_toysTP.gridx = 0;
		gbc_toysTP.gridy = 0;
		contentPanel.add(toysTP, gbc_toysTP);
	
		JComboBox toysCB = new JComboBox();
		String[] CBFiller = new String[GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().size()];
		for (int i=0; i<GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().size();i++) {
			CBFiller[i]=GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().get(i).getClass().getName();
		}
		toysCB.setModel(new DefaultComboBoxModel(CBFiller));
		GridBagConstraints gbc_toysCB = new GridBagConstraints();
		gbc_toysCB.insets = new Insets(0, 0, 5, 0);
		gbc_toysCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_toysCB.gridx = 0;
		gbc_toysCB.gridy = 5;
		contentPanel.add(toysCB, gbc_toysCB);
		
		JLabel actionsLbl = new JLabel("Sorry this pet has ran out of actions");
		GridBagConstraints gbc_actionsLbl = new GridBagConstraints();
		gbc_actionsLbl.insets = new Insets(0, 0, 5, 0);
		gbc_actionsLbl.gridx = 0;
		gbc_actionsLbl.gridy = 6;
		contentPanel.add(actionsLbl, gbc_actionsLbl);
		actionsLbl.setVisible(false);
		
		JButton playBtn = new JButton("play");
		playBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Boolean valid = false;
				if (pet==0) {
					if(GameEnv.getActions1()>0) {
						GameEnv.setActions1(GameEnv.getActions1()-1);
						valid=true;
					}
				}else if (pet==1) {
					if(GameEnv.getActions2()>0) {
						GameEnv.setActions2(GameEnv.getActions2()-1);
						valid=true;
					}
				}else if(pet==2) {
					if(GameEnv.getActions3()>0) {
						GameEnv.setActions3(GameEnv.getActions3()-1);
						valid=true;
					}
				}
				if(toysCB.getSelectedIndex()==-1){
					valid=false;
					actionsLbl.setText("You have no more Toys");
				}
				if (valid){
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(pet).play(player.getToys().get(toysCB.getSelectedIndex()));
					if (player.getToys().get(toysCB.getSelectedIndex()).getDuribility()<=0){
						player.getToys().remove(toysCB.getSelectedIndex());
					}
					
					String[] CBFiller = new String[GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().size()];
					for (int i=0; i<GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().size();i++) {
						CBFiller[i]=GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getToys().get(i).getClass().getName();
					}
					toysCB.setModel(new DefaultComboBoxModel(CBFiller));
				}else{
					actionsLbl.setVisible(true);
				}
			}
		});
		
		GridBagConstraints gbc_playBtn = new GridBagConstraints();
		gbc_playBtn.gridx = 0;
		gbc_playBtn.gridy = 7;
		contentPanel.add(playBtn, gbc_playBtn);
	
	}

}
