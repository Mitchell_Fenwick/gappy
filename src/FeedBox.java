import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class FeedBox extends JDialog {
	private int pet = GameEnv.getCurrentPet();
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FeedBox dialog = new FeedBox();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FeedBox() {
		setBounds(100, 100, 300 , 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
	
		JTextPane foodTP = new JTextPane();
		GridBagConstraints gbc_foodTP = new GridBagConstraints();
		gbc_foodTP.gridheight = 5;
		gbc_foodTP.insets = new Insets(0, 0, 5, 0);
		gbc_foodTP.fill = GridBagConstraints.BOTH;
		gbc_foodTP.gridx = 0;
		gbc_foodTP.gridy = 0;
		contentPanel.add(foodTP, gbc_foodTP);
	
		JComboBox foodCB = new JComboBox();
		String[] CBFiller = new String[GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().size()];
		for (int i=0; i<GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().size();i++) {
			CBFiller[i]=GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().get(i).getClass().getName();
		}
		foodCB.setModel(new DefaultComboBoxModel(CBFiller));
		GridBagConstraints gbc_foodCB = new GridBagConstraints();
		gbc_foodCB.insets = new Insets(0, 0, 5, 0);
		gbc_foodCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_foodCB.gridx = 0;
		gbc_foodCB.gridy = 5;
		contentPanel.add(foodCB, gbc_foodCB);
		
		JLabel actionsLbl = new JLabel("Sorry this pet has ran out of actions");
		GridBagConstraints gbc_actionsLbl = new GridBagConstraints();
		gbc_actionsLbl.insets = new Insets(0, 0, 5, 0);
		gbc_actionsLbl.gridx = 0;
		gbc_actionsLbl.gridy = 6;
		contentPanel.add(actionsLbl, gbc_actionsLbl);
		actionsLbl.setVisible(false);
		
		JButton feedBtn = new JButton("Feed");
		feedBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Boolean valid = false;
				if (pet==0) {
					if(GameEnv.getActions1()>0) {
						GameEnv.setActions1(GameEnv.getActions1()-1);
						valid=true;
					}
				}else if (pet==1) {
					if(GameEnv.getActions2()>0) {
						GameEnv.setActions2(GameEnv.getActions2()-1);
						valid=true;
					}
				}else if(pet==2) {
					if(GameEnv.getActions3()>0) {
						GameEnv.setActions3(GameEnv.getActions3()-1);
						valid=true;
					}
				}
				if(foodCB.getSelectedIndex()==-1){
					valid=false;
					actionsLbl.setText("You have no more food");
				}
				if (valid){
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(pet).feed(player.getFood().get(foodCB.getSelectedIndex()));
					player.getFood().remove(foodCB.getSelectedIndex());
					
					String[] CBFiller = new String[GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().size()];
					for (int i=0; i<GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().size();i++) {
						CBFiller[i]=GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getFood().get(i).getClass().getName();
					}
					foodCB.setModel(new DefaultComboBoxModel(CBFiller));
				}else{
					actionsLbl.setVisible(true);
				}
			}
		});
		
		GridBagConstraints gbc_feedBtn = new GridBagConstraints();
		gbc_feedBtn.gridx = 0;
		gbc_feedBtn.gridy = 7;
		contentPanel.add(feedBtn, gbc_feedBtn);
	
	
	}

}
