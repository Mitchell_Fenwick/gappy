

import java.net.*;
import java.io.*;

public class URLCon {

   public static void request(String urll) {
      try {
         URL url = new URL(urll);
         URLConnection urlConnection = url.openConnection();
         HttpURLConnection connection = null;
         if(urlConnection instanceof HttpURLConnection) {
            connection = (HttpURLConnection) urlConnection;
         }
         
         BufferedReader in = new BufferedReader(
            new InputStreamReader(connection.getInputStream()));
         String urlString = "";
         String current;
         
         while((current = in.readLine()) != null) {
            urlString += current;
         }
      }catch(IOException e) {
         e.printStackTrace();
      }
   }
}