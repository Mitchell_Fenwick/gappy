
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.Component;
import org.json.JSONException;
import org.json.JSONObject;

public class GUI {

	private JFrame frame;
	private JTextField nameTF;
	private JTextField pet1TF;
	private JTextField pet2TF;
	private JTextField pet3TF;
	private int pin;
	public JLabel lblPlayers;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
		Store.init();
		GameEnv.petsInitialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel cardPnl = new JPanel();
		CardLayout cardLayout = new CardLayout(0,0);
		frame.setContentPane(cardPnl);
		frame.getContentPane().setLayout(cardLayout);
		
		JPanel homePnl = new JPanel();
		cardPnl.add(homePnl, "home");
		GridBagLayout gbl_homePnl = new GridBagLayout();
		gbl_homePnl.columnWidths = new int[]{169, 0};
		gbl_homePnl.rowHeights = new int[]{39, 25, 33, 33, 33, 33, 0};
		gbl_homePnl.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_homePnl.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		homePnl.setLayout(gbl_homePnl);
		
		JPanel playerSelectPnl = new JPanel();
		cardPnl.add(playerSelectPnl, "playerSelect");
		GridBagLayout gbl_playerSelectPnl = new GridBagLayout();
		gbl_playerSelectPnl.columnWidths = new int[]{0, 0, 100, 0};
		gbl_playerSelectPnl.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_playerSelectPnl.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_playerSelectPnl.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		playerSelectPnl.setLayout(gbl_playerSelectPnl);
		
		JPanel createOfflinePnl = new JPanel();
		cardPnl.add(createOfflinePnl, "createOffline");
		GridBagLayout gbl_createOfflinePnl = new GridBagLayout();
		gbl_createOfflinePnl.columnWidths = new int[]{294, 0};
		gbl_createOfflinePnl.rowHeights = new int[]{0, 0, 0, 0, 29, 32, 0};
		gbl_createOfflinePnl.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_createOfflinePnl.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		createOfflinePnl.setLayout(gbl_createOfflinePnl);
		
		JPanel dayPnl = new JPanel();
		cardPnl.add(dayPnl, "name_2533873964821305");
		GridBagLayout gbl_dayPnl = new GridBagLayout();
		gbl_dayPnl.columnWidths = new int[]{206, 92, 92, 0, 0};
		gbl_dayPnl.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_dayPnl.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_dayPnl.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		dayPnl.setLayout(gbl_dayPnl);
		
		JLabel currentDayLbl = new JLabel("Day 1");
		currentDayLbl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_currentDayLbl = new GridBagConstraints();
		gbc_currentDayLbl.gridwidth = 2;
		gbc_currentDayLbl.insets = new Insets(0, 0, 5, 5);
		gbc_currentDayLbl.gridx = 1;
		gbc_currentDayLbl.gridy = 0;
		dayPnl.add(currentDayLbl, gbc_currentDayLbl);
		
		JLabel playerPetLbl = new JLabel("Player, what would you like to do with your pet, pet");
		GridBagConstraints gbc_playerPetLbl = new GridBagConstraints();
		gbc_playerPetLbl.gridwidth = 4;
		gbc_playerPetLbl.insets = new Insets(0, 0, 5, 0);
		gbc_playerPetLbl.gridx = 0;
		gbc_playerPetLbl.gridy = 1;
		dayPnl.add(playerPetLbl, gbc_playerPetLbl);
		
		JButton offlineBtn = new JButton("Play offline");
		offlineBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardLayout.show(cardPnl, "createOffline");
			}
		});
		
		JLabel welcomeLbl = new JLabel("Welcome to game!");
		GridBagConstraints gbc_welcomeLbl = new GridBagConstraints();
		gbc_welcomeLbl.anchor = GridBagConstraints.NORTH;
		gbc_welcomeLbl.insets = new Insets(0, 0, 5, 0);
		gbc_welcomeLbl.gridx = 0;
		gbc_welcomeLbl.gridy = 1;
		homePnl.add(welcomeLbl, gbc_welcomeLbl);
		welcomeLbl.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_offlineBtn = new GridBagConstraints();
		gbc_offlineBtn.fill = GridBagConstraints.VERTICAL;
		gbc_offlineBtn.insets = new Insets(0, 0, 5, 0);
		gbc_offlineBtn.gridx = 0;
		gbc_offlineBtn.gridy = 2;
		homePnl.add(offlineBtn, gbc_offlineBtn);
		
		JButton helpBtn = new JButton("Help");
		GridBagConstraints gbc_helpBtn = new GridBagConstraints();
		gbc_helpBtn.insets = new Insets(0, 0, 5, 0);
		gbc_helpBtn.fill = GridBagConstraints.VERTICAL;
		gbc_helpBtn.gridx = 0;
		gbc_helpBtn.gridy = 4;
		homePnl.add(helpBtn, gbc_helpBtn);
		
		JLabel playersLbl = new JLabel("Number of players");
		playersLbl.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_playersLbl = new GridBagConstraints();
		gbc_playersLbl.insets = new Insets(0, 0, 5, 0);
		gbc_playersLbl.gridx = 0;
		gbc_playersLbl.gridy = 0;
		createOfflinePnl.add(playersLbl, gbc_playersLbl);
		
		JComboBox playersCB = new JComboBox();
		playersCB.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3"}));
		playersCB.setMaximumRowCount(8);
		playersCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_playersCB = new GridBagConstraints();
		gbc_playersCB.insets = new Insets(0, 0, 5, 0);
		gbc_playersCB.gridx = 0;
		gbc_playersCB.gridy = 1;
		createOfflinePnl.add(playersCB, gbc_playersCB);
		
		JLabel daysLbl = new JLabel("Number of days");
		daysLbl.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_daysLbl = new GridBagConstraints();
		gbc_daysLbl.insets = new Insets(0, 0, 5, 0);
		gbc_daysLbl.gridx = 0;
		gbc_daysLbl.gridy = 2;
		createOfflinePnl.add(daysLbl, gbc_daysLbl);
		
		JComboBox daysCB = new JComboBox();
		daysCB.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		daysCB.setMaximumRowCount(8);
		daysCB.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_daysCB = new GridBagConstraints();
		gbc_daysCB.insets = new Insets(0, 0, 5, 0);
		gbc_daysCB.gridx = 0;
		gbc_daysCB.gridy = 3;
		createOfflinePnl.add(daysCB, gbc_daysCB);
	
		JLabel playerNumLbl = new JLabel("Player ");
		playerNumLbl.setFont(new Font("Sitka Text", Font.PLAIN, 15));
		GridBagConstraints gbc_playerNumLbl = new GridBagConstraints();
		gbc_playerNumLbl.insets = new Insets(0, 0, 5, 5);
		gbc_playerNumLbl.gridx = 1;
		gbc_playerNumLbl.gridy = 0;
		playerSelectPnl.add(playerNumLbl, gbc_playerNumLbl);
		
		JButton createBtn = new JButton("Create game");
		createBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GameEnv.setPlayerNum(playersCB.getSelectedIndex()+1);
				GameEnv.setDays(daysCB.getSelectedIndex()+1);
				playerNumLbl.setText(("Player "+(GameEnv.getPlayers().size()+1)));
				cardLayout.show(cardPnl, "playerSelect");
			}
		});
		GridBagConstraints gbc_createBtn = new GridBagConstraints();
		gbc_createBtn.fill = GridBagConstraints.VERTICAL;
		gbc_createBtn.gridx = 0;
		gbc_createBtn.gridy = 5;
		createOfflinePnl.add(createBtn, gbc_createBtn);
		
		JLabel nameLbl = new JLabel("Player name");
		GridBagConstraints gbc_nameLbl = new GridBagConstraints();
		gbc_nameLbl.insets = new Insets(0, 0, 5, 5);
		gbc_nameLbl.gridx = 1;
		gbc_nameLbl.gridy = 1;
		playerSelectPnl.add(nameLbl, gbc_nameLbl);
		
		nameTF = new JTextField();
		GridBagConstraints gbc_nameTF = new GridBagConstraints();
		gbc_nameTF.insets = new Insets(0, 0, 5, 5);
		gbc_nameTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameTF.gridx = 1;
		gbc_nameTF.gridy = 2;
		playerSelectPnl.add(nameTF, gbc_nameTF);
		nameTF.setColumns(10);
		
		JLabel selectPetLbl = new JLabel("Select your pets and names");
		GridBagConstraints gbc_selectPetLbl = new GridBagConstraints();
		gbc_selectPetLbl.insets = new Insets(0, 0, 5, 5);
		gbc_selectPetLbl.gridx = 1;
		gbc_selectPetLbl.gridy = 3;
		playerSelectPnl.add(selectPetLbl, gbc_selectPetLbl);
		
		pet1TF = new JTextField();
		pet1TF.setText("Pet1");
		GridBagConstraints gbc_pet1TF = new GridBagConstraints();
		gbc_pet1TF.insets = new Insets(0, 0, 5, 5);
		gbc_pet1TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet1TF.gridx = 0;
		gbc_pet1TF.gridy = 4;
		playerSelectPnl.add(pet1TF, gbc_pet1TF);
		pet1TF.setColumns(10);
		
		pet2TF = new JTextField();
		pet2TF.setText("Pet2");
		GridBagConstraints gbc_pet2TF = new GridBagConstraints();
		gbc_pet2TF.insets = new Insets(0, 0, 5, 5);
		gbc_pet2TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet2TF.gridx = 1;
		gbc_pet2TF.gridy = 4;
		playerSelectPnl.add(pet2TF, gbc_pet2TF);
		pet2TF.setColumns(10);
		
		pet3TF = new JTextField();
		pet3TF.setText("Pet3");
		GridBagConstraints gbc_pet3TF = new GridBagConstraints();
		gbc_pet3TF.insets = new Insets(0, 0, 5, 0);
		gbc_pet3TF.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet3TF.gridx = 2;
		gbc_pet3TF.gridy = 4;
		playerSelectPnl.add(pet3TF, gbc_pet3TF);
		pet3TF.setColumns(10);
		
		
		
		JLabel averageWeight1Lbl = new JLabel("Average Weight: ");
		GridBagConstraints gbc_averageWeight1Lbl = new GridBagConstraints();
		gbc_averageWeight1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_averageWeight1Lbl.gridx = 0;
		gbc_averageWeight1Lbl.gridy = 6;
		playerSelectPnl.add(averageWeight1Lbl, gbc_averageWeight1Lbl);
		
		JLabel averageWeight2Lbl = new JLabel("Average Weight: ");
		GridBagConstraints gbc_averageWeight2Lbl = new GridBagConstraints();
		gbc_averageWeight2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_averageWeight2Lbl.gridx = 1;
		gbc_averageWeight2Lbl.gridy = 6;
		playerSelectPnl.add(averageWeight2Lbl, gbc_averageWeight2Lbl);
		
		JLabel averageWeight3Lbl = new JLabel("Average Weight: ");
		GridBagConstraints gbc_averageWeight3Lbl = new GridBagConstraints();
		gbc_averageWeight3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_averageWeight3Lbl.gridx = 2;
		gbc_averageWeight3Lbl.gridy = 6;
		playerSelectPnl.add(averageWeight3Lbl, gbc_averageWeight3Lbl);
		
		JLabel FavouriteToy1Lbl = new JLabel("Favourite Toy: ");
		GridBagConstraints gbc_FavouriteToy1Lbl = new GridBagConstraints();
		gbc_FavouriteToy1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_FavouriteToy1Lbl.gridx = 0;
		gbc_FavouriteToy1Lbl.gridy = 7;
		playerSelectPnl.add(FavouriteToy1Lbl, gbc_FavouriteToy1Lbl);
		
		JLabel FavouriteToy2Lbl = new JLabel("Favourite Toy: ");
		GridBagConstraints gbc_FavouriteToy2Lbl = new GridBagConstraints();
		gbc_FavouriteToy2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_FavouriteToy2Lbl.gridx = 1;
		gbc_FavouriteToy2Lbl.gridy = 7;
		playerSelectPnl.add(FavouriteToy2Lbl, gbc_FavouriteToy2Lbl);
		
		JLabel FavouriteToy3Lbl = new JLabel("Favourite Toy: ");
		GridBagConstraints gbc_FavouriteToy3Lbl = new GridBagConstraints();
		gbc_FavouriteToy3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_FavouriteToy3Lbl.gridx = 2;
		gbc_FavouriteToy3Lbl.gridy = 7;
		playerSelectPnl.add(FavouriteToy3Lbl, gbc_FavouriteToy3Lbl);
		
		JLabel FavouriteFood1Lbl = new JLabel("Favourite Food:");
		GridBagConstraints gbc_FavouriteFood1Lbl = new GridBagConstraints();
		gbc_FavouriteFood1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_FavouriteFood1Lbl.gridx = 0;
		gbc_FavouriteFood1Lbl.gridy = 8;
		playerSelectPnl.add(FavouriteFood1Lbl, gbc_FavouriteFood1Lbl);
		
		JLabel FavouriteFood2Lbl = new JLabel("Favourite Food:");
		GridBagConstraints gbc_FavouriteFood2Lbl = new GridBagConstraints();
		gbc_FavouriteFood2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_FavouriteFood2Lbl.gridx = 1;
		gbc_FavouriteFood2Lbl.gridy = 8;
		playerSelectPnl.add(FavouriteFood2Lbl, gbc_FavouriteFood2Lbl);
		
		JLabel FavouriteFood3Lbl = new JLabel("Favourite Food:");
		GridBagConstraints gbc_FavouriteFood3Lbl = new GridBagConstraints();
		gbc_FavouriteFood3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_FavouriteFood3Lbl.gridx = 2;
		gbc_FavouriteFood3Lbl.gridy = 8;
		playerSelectPnl.add(FavouriteFood3Lbl, gbc_FavouriteFood3Lbl);
		
		JLabel hunger1Lbl = new JLabel("Hunger: ");
		GridBagConstraints gbc_hunger1Lbl = new GridBagConstraints();
		gbc_hunger1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_hunger1Lbl.gridx = 0;
		gbc_hunger1Lbl.gridy = 9;
		playerSelectPnl.add(hunger1Lbl, gbc_hunger1Lbl);
		
		JLabel hunger2Lbl = new JLabel("Hunger: ");
		GridBagConstraints gbc_hunger2Lbl = new GridBagConstraints();
		gbc_hunger2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_hunger2Lbl.gridx = 1;
		gbc_hunger2Lbl.gridy = 9;
		playerSelectPnl.add(hunger2Lbl, gbc_hunger2Lbl);
		
		JLabel hunger3Lbl = new JLabel("Hunger: ");
		GridBagConstraints gbc_hunger3Lbl = new GridBagConstraints();
		gbc_hunger3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_hunger3Lbl.gridx = 2;
		gbc_hunger3Lbl.gridy = 9;
		playerSelectPnl.add(hunger3Lbl, gbc_hunger3Lbl);
		
		JLabel noPetLbl = new JLabel("You have to select Atleast one pet!");
		noPetLbl.setForeground(Color.RED);
		GridBagConstraints gbc_noPetLbl = new GridBagConstraints();
		gbc_noPetLbl.insets = new Insets(0, 0, 5, 5);
		gbc_noPetLbl.gridx = 1;
		gbc_noPetLbl.gridy = 10;
		playerSelectPnl.add(noPetLbl, gbc_noPetLbl);
		noPetLbl.setVisible(false);
		
		JComboBox pet1CB = new JComboBox();
		pet1CB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pet1CB.getSelectedIndex()==0) {
					averageWeight1Lbl.setText("Average Weight: ");
					FavouriteToy1Lbl.setText("Favourite Toy: ");
					FavouriteFood1Lbl.setText("Favourite Food: ");
					hunger1Lbl.setText("Hunger: ");
				}else{
					averageWeight1Lbl.setText("Average Weight: "+GameEnv.getPets().get(pet1CB.getSelectedIndex()-1).getWeight());
					FavouriteToy1Lbl.setText("Favourite Toy: "+GameEnv.getPets().get(pet1CB.getSelectedIndex()-1).getFavToy());
					FavouriteFood1Lbl.setText("Favourite Food: "+GameEnv.getPets().get(pet1CB.getSelectedIndex()-1).getFavFood());
					hunger1Lbl.setText("Hunger: "+GameEnv.getPets().get(pet1CB.getSelectedIndex()-1).getHunger());
				}
			}
		});
		pet1CB.setModel(new DefaultComboBoxModel(new String[] {"None", "Dog", "Cat", "Chinchilla", "Snake", "Bear", "Tiger"}));
		GridBagConstraints gbc_pet1CB = new GridBagConstraints();
		gbc_pet1CB.insets = new Insets(0, 0, 5, 5);
		gbc_pet1CB.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet1CB.gridx = 0;
		gbc_pet1CB.gridy = 5;
		playerSelectPnl.add(pet1CB, gbc_pet1CB);
		
		JComboBox pet2CB = new JComboBox();
		pet2CB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pet2CB.getSelectedIndex()==0) {
					averageWeight2Lbl.setText("Average Weight: ");
					FavouriteToy2Lbl.setText("Favourite Toy: ");
					FavouriteFood2Lbl.setText("Favourite Food: ");
					hunger2Lbl.setText("Hunger: ");
				}else{
					averageWeight2Lbl.setText("Average Weight: "+GameEnv.getPets().get(pet2CB.getSelectedIndex()-1).getWeight());
					FavouriteToy2Lbl.setText("Favourite Toy: "+GameEnv.getPets().get(pet2CB.getSelectedIndex()-1).getFavToy());
					FavouriteFood2Lbl.setText("Favourite Food: "+GameEnv.getPets().get(pet2CB.getSelectedIndex()-1).getFavFood());
					hunger2Lbl.setText("Hunger: "+GameEnv.getPets().get(pet2CB.getSelectedIndex()-1).getHunger());
				}
			}
		});
		pet2CB.setModel(new DefaultComboBoxModel(new String[] {"None", "Dog", "Cat", "Chinchilla", "Snake", "Bear", "Tiger"}));
		GridBagConstraints gbc_pet2CB = new GridBagConstraints();
		gbc_pet2CB.insets = new Insets(0, 0, 5, 5);
		gbc_pet2CB.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet2CB.gridx = 1;
		gbc_pet2CB.gridy = 5;
		playerSelectPnl.add(pet2CB, gbc_pet2CB);
		
		JComboBox pet3CB = new JComboBox();
		pet3CB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pet3CB.getSelectedIndex()==0) {
					averageWeight3Lbl.setText("Average Weight: ");
					FavouriteToy3Lbl.setText("Favourite Toy: ");
					FavouriteFood3Lbl.setText("Favourite Food: ");
					hunger3Lbl.setText("Hunger: ");
				}else{
					averageWeight3Lbl.setText("Average Weight: "+GameEnv.getPets().get(pet3CB.getSelectedIndex()-1).getWeight());
					FavouriteToy3Lbl.setText("Favourite Toy: "+GameEnv.getPets().get(pet3CB.getSelectedIndex()-1).getFavToy());
					FavouriteFood3Lbl.setText("Favourite Food: "+GameEnv.getPets().get(pet3CB.getSelectedIndex()-1).getFavFood());
					hunger3Lbl.setText("Hunger: "+GameEnv.getPets().get(pet3CB.getSelectedIndex()-1).getHunger());
			
				}
			}
		});
		pet3CB.setModel(new DefaultComboBoxModel(new String[] {"None", "Dog", "Cat", "Chinchilla", "Snake", "Bear", "Tiger"}));
		GridBagConstraints gbc_pet3CB = new GridBagConstraints();
		gbc_pet3CB.insets = new Insets(0, 0, 5, 0);
		gbc_pet3CB.fill = GridBagConstraints.HORIZONTAL;
		gbc_pet3CB.gridx = 2;
		gbc_pet3CB.gridy = 5;
		playerSelectPnl.add(pet3CB, gbc_pet3CB);
		
		
		JButton btnVisitStore = new JButton("Visit Store");
		btnVisitStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				StoreBox store = new StoreBox();
				store.setVisible(true);
				store.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
				    }
				});
			}
		});
		GridBagConstraints gbc_btnVisitStore = new GridBagConstraints();
		gbc_btnVisitStore.gridwidth = 2;
		gbc_btnVisitStore.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnVisitStore.insets = new Insets(0, 0, 5, 5);
		gbc_btnVisitStore.gridx = 1;
		gbc_btnVisitStore.gridy = 2;
		dayPnl.add(btnVisitStore, gbc_btnVisitStore);
		
		JPanel endGamePnl = new JPanel();
		cardPnl.add(endGamePnl, "name_2875242434973484");
		GridBagLayout gbl_endGamePnl = new GridBagLayout();
		gbl_endGamePnl.columnWidths = new int[]{0, 0};
		gbl_endGamePnl.rowHeights = new int[]{0, 0, 0, 0};
		gbl_endGamePnl.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_endGamePnl.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		endGamePnl.setLayout(gbl_endGamePnl);
		
		JLabel endGameLbl = new JLabel("Game scores!");
		GridBagConstraints gbc_endGameLbl = new GridBagConstraints();
		gbc_endGameLbl.insets = new Insets(0, 0, 5, 0);
		gbc_endGameLbl.gridx = 0;
		gbc_endGameLbl.gridy = 0;
		endGamePnl.add(endGameLbl, gbc_endGameLbl);
		
		JLabel elseLbl = new JLabel("elseLbl");
		GridBagConstraints gbc_elseLbl = new GridBagConstraints();
		gbc_elseLbl.insets = new Insets(0, 0, 5, 0);
		gbc_elseLbl.gridx = 0;
		gbc_elseLbl.gridy = 1;
		endGamePnl.add(elseLbl, gbc_elseLbl);
		
		JLabel petName1Lbl = new JLabel("Pet: ");
		petName1Lbl.setVisible(false);
		GridBagConstraints gbc_petName1Lbl = new GridBagConstraints();
		gbc_petName1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petName1Lbl.gridx = 0;
		gbc_petName1Lbl.gridy = 3;
		dayPnl.add(petName1Lbl, gbc_petName1Lbl);
		
		JLabel petName2Lbl = new JLabel("Pet:");
		GridBagConstraints gbc_petName2Lbl = new GridBagConstraints();
		gbc_petName2Lbl.gridwidth = 2;
		gbc_petName2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petName2Lbl.gridx = 1;
		gbc_petName2Lbl.gridy = 3;
		dayPnl.add(petName2Lbl, gbc_petName2Lbl);
		
		JLabel petName3Lbl = new JLabel("Pet:");
		petName3Lbl.setVisible(false);
		petName2Lbl.setVisible(false);
		GridBagConstraints gbc_petName3Lbl = new GridBagConstraints();
		gbc_petName3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petName3Lbl.gridx = 3;
		gbc_petName3Lbl.gridy = 3;
		dayPnl.add(petName3Lbl, gbc_petName3Lbl);
		
		JLabel petType1Lbl = new JLabel("Type: ");
		GridBagConstraints gbc_petType1Lbl = new GridBagConstraints();
		gbc_petType1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petType1Lbl.gridx = 0;
		gbc_petType1Lbl.gridy = 4;
		dayPnl.add(petType1Lbl, gbc_petType1Lbl);
		
		JLabel petType2Lbl = new JLabel("Type: ");
		GridBagConstraints gbc_petType2Lbl = new GridBagConstraints();
		gbc_petType2Lbl.gridwidth = 2;
		gbc_petType2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petType2Lbl.gridx = 1;
		gbc_petType2Lbl.gridy = 4;
		dayPnl.add(petType2Lbl, gbc_petType2Lbl);
		
		JLabel petType3Lbl = new JLabel("Type: ");
		GridBagConstraints gbc_petType3Lbl = new GridBagConstraints();
		gbc_petType3Lbl.anchor = GridBagConstraints.NORTH;
		gbc_petType3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petType3Lbl.gridx = 3;
		gbc_petType3Lbl.gridy = 4;
		dayPnl.add(petType3Lbl, gbc_petType3Lbl);
		
		petType1Lbl.setVisible(false);
		petType2Lbl.setVisible(false);
		petType3Lbl.setVisible(false);
		
		
		JLabel petWeight1Lbl = new JLabel("Weight:");
		GridBagConstraints gbc_petWeight1Lbl = new GridBagConstraints();
		gbc_petWeight1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petWeight1Lbl.gridx = 0;
		gbc_petWeight1Lbl.gridy = 5;
		dayPnl.add(petWeight1Lbl, gbc_petWeight1Lbl);
		
		JLabel petWeight2Lbl = new JLabel("Weight:");
		GridBagConstraints gbc_petWeight2Lbl = new GridBagConstraints();
		gbc_petWeight2Lbl.gridwidth = 2;
		gbc_petWeight2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petWeight2Lbl.gridx = 1;
		gbc_petWeight2Lbl.gridy = 5;
		dayPnl.add(petWeight2Lbl, gbc_petWeight2Lbl);
		
		JLabel petWeight3Lbl = new JLabel("Weight");
		GridBagConstraints gbc_petWeight3Lbl = new GridBagConstraints();
		gbc_petWeight3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petWeight3Lbl.gridx = 3;
		gbc_petWeight3Lbl.gridy = 5;
		dayPnl.add(petWeight3Lbl, gbc_petWeight3Lbl);
	
		petWeight1Lbl.setVisible(false);
		petWeight2Lbl.setVisible(false);
		petWeight3Lbl.setVisible(false);
		
		JLabel petHunger1Lbl = new JLabel("Hunger: ");
		petHunger1Lbl.setVisible(false);
		GridBagConstraints gbc_petHunger1Lbl = new GridBagConstraints();
		gbc_petHunger1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petHunger1Lbl.gridx = 0;
		gbc_petHunger1Lbl.gridy = 6;
		dayPnl.add(petHunger1Lbl, gbc_petHunger1Lbl);
		
		JLabel petHunger2Lbl = new JLabel("Hunger: ");
		GridBagConstraints gbc_petHunger2Lbl = new GridBagConstraints();
		gbc_petHunger2Lbl.gridwidth = 2;
		gbc_petHunger2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petHunger2Lbl.gridx = 1;
		gbc_petHunger2Lbl.gridy = 6;
		dayPnl.add(petHunger2Lbl, gbc_petHunger2Lbl);
		
		JLabel petHunger3Lbl = new JLabel("Hunger: ");
		GridBagConstraints gbc_petHunger3Lbl = new GridBagConstraints();
		gbc_petHunger3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petHunger3Lbl.gridx = 3;
		gbc_petHunger3Lbl.gridy = 6;
		dayPnl.add(petHunger3Lbl, gbc_petHunger3Lbl);
		petHunger2Lbl.setVisible(false);
		petHunger3Lbl.setVisible(false);
		
		JLabel actions1Lbl = new JLabel("No more actions");
		actions1Lbl.setVisible(false);
		GridBagConstraints gbc_actions1Lbl = new GridBagConstraints();
		gbc_actions1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_actions1Lbl.gridx = 0;
		gbc_actions1Lbl.gridy = 14;
		dayPnl.add(actions1Lbl, gbc_actions1Lbl);
		
		JLabel actions2Lbl = new JLabel("No more actions");
		actions2Lbl.setVisible(false);
		GridBagConstraints gbc_actions2Lbl = new GridBagConstraints();
		gbc_actions2Lbl.gridwidth = 2;
		gbc_actions2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_actions2Lbl.gridx = 1;
		gbc_actions2Lbl.gridy = 14;
		dayPnl.add(actions2Lbl, gbc_actions2Lbl);
		
		JLabel actions3Lbl = new JLabel("No more actions");
		actions3Lbl.setVisible(false);
		GridBagConstraints gbc_actions3Lbl = new GridBagConstraints();
		gbc_actions3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_actions3Lbl.gridx = 3;
		gbc_actions3Lbl.gridy = 14;
		dayPnl.add(actions3Lbl, gbc_actions3Lbl);
		
		JLabel petSickness1Lbl = new JLabel("Sickness:");
		petSickness1Lbl.setVisible(false);
		GridBagConstraints gbc_petSickness1Lbl = new GridBagConstraints();
		gbc_petSickness1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petSickness1Lbl.gridx = 0;
		gbc_petSickness1Lbl.gridy = 7;
		dayPnl.add(petSickness1Lbl, gbc_petSickness1Lbl);
		
		JLabel petSickness2Lbl = new JLabel("Sickness:");
		petSickness2Lbl.setVisible(false);
		GridBagConstraints gbc_petSickness2Lbl = new GridBagConstraints();
		gbc_petSickness2Lbl.gridwidth = 2;
		gbc_petSickness2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petSickness2Lbl.gridx = 1;
		gbc_petSickness2Lbl.gridy = 7;
		dayPnl.add(petSickness2Lbl, gbc_petSickness2Lbl);
		
		JLabel petSickness3Lbl = new JLabel("Sickness:");
		petSickness3Lbl.setVisible(false);
		GridBagConstraints gbc_petSickness3Lbl = new GridBagConstraints();
		gbc_petSickness3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petSickness3Lbl.gridx = 3;
		gbc_petSickness3Lbl.gridy = 7;
		dayPnl.add(petSickness3Lbl, gbc_petSickness3Lbl);
		
		
		JLabel petTiredness1Lbl = new JLabel("Tiredness");
		petTiredness1Lbl.setVisible(false);
		GridBagConstraints gbc_petTiredness1Lbl = new GridBagConstraints();
		gbc_petTiredness1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petTiredness1Lbl.gridx = 0;
		gbc_petTiredness1Lbl.gridy = 8;
		dayPnl.add(petTiredness1Lbl, gbc_petTiredness1Lbl);
		
		JLabel petTiredness2Lbl = new JLabel("Tiredness");
		petTiredness2Lbl.setVisible(false);
		GridBagConstraints gbc_petTiredness2Lbl = new GridBagConstraints();
		gbc_petTiredness2Lbl.gridwidth = 2;
		gbc_petTiredness2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petTiredness2Lbl.gridx = 1;
		gbc_petTiredness2Lbl.gridy = 8;
		dayPnl.add(petTiredness2Lbl, gbc_petTiredness2Lbl);
		
		JLabel petTiredness3Lbl = new JLabel("Tiredness");
		petTiredness3Lbl.setVisible(false);
		GridBagConstraints gbc_petTiredness3Lbl = new GridBagConstraints();
		gbc_petTiredness3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petTiredness3Lbl.gridx = 3;
		gbc_petTiredness3Lbl.gridy = 8;
		dayPnl.add(petTiredness3Lbl, gbc_petTiredness3Lbl);
		
		JLabel petToilet1Lbl = new JLabel("Toilet Level");
		petToilet1Lbl.setVisible(false);
		GridBagConstraints gbc_petToilet1Lbl = new GridBagConstraints();
		gbc_petToilet1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petToilet1Lbl.gridx = 0;
		gbc_petToilet1Lbl.gridy = 9;
		dayPnl.add(petToilet1Lbl, gbc_petToilet1Lbl);
		
		JLabel petToilet2Lbl = new JLabel("Toilet Level");
		petToilet2Lbl.setVisible(false);
		GridBagConstraints gbc_petToilet2Lbl = new GridBagConstraints();
		gbc_petToilet2Lbl.gridwidth = 2;
		gbc_petToilet2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_petToilet2Lbl.gridx = 1;
		gbc_petToilet2Lbl.gridy = 9;
		dayPnl.add(petToilet2Lbl, gbc_petToilet2Lbl);
		
		JLabel petToilet3Lbl = new JLabel("Toilet Level");
		petToilet3Lbl.setVisible(false);
		GridBagConstraints gbc_petToilet3Lbl = new GridBagConstraints();
		gbc_petToilet3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_petToilet3Lbl.gridx = 3;
		gbc_petToilet3Lbl.gridy = 9;
		dayPnl.add(petToilet3Lbl, gbc_petToilet3Lbl);
		
		JButton sleep1Btn = new JButton("Sleep");
		sleep1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (GameEnv.getActions1()>0){
					GameEnv.setActions1(GameEnv.getActions1()-1);
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(0).sleep();
					petName1Lbl.setText(player.getPets().get(0).getName());
					petType1Lbl.setText(player.getPets().get(0).getClass().getSimpleName());
					String[] info = player.getPets().get(0).getInfo();
					petWeight1Lbl.setText("Weight: "+info[0]);
					petHunger1Lbl.setText("Hunger: "+info[1]);
					petSickness1Lbl.setText("Sickness: "+info[2]);
					petTiredness1Lbl.setText("Tiredness: "+info[3]);
					petToilet1Lbl.setText("Toilet: "+info[4]);
				}else{
					actions1Lbl.setVisible(true);
				}
			}
		});
		sleep1Btn.setVisible(false);
		GridBagConstraints gbc_sleep1Btn = new GridBagConstraints();
		gbc_sleep1Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_sleep1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_sleep1Btn.gridx = 0;
		gbc_sleep1Btn.gridy = 12;
		dayPnl.add(sleep1Btn, gbc_sleep1Btn);
		
		JButton sleep2Btn = new JButton("Sleep");
		sleep2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (GameEnv.getActions2()>0){
					GameEnv.setActions2(GameEnv.getActions2()-1);
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(1).sleep();
					petName2Lbl.setText(player.getPets().get(1).getName());
					petType2Lbl.setText(player.getPets().get(1).getClass().getSimpleName());
					String[] info = player.getPets().get(1).getInfo();
					petWeight2Lbl.setText("Weight: "+info[0]);
					petHunger2Lbl.setText("Hunger: "+info[1]);
					petSickness2Lbl.setText("Sickness: "+info[2]);
					petTiredness2Lbl.setText("Tiredness: "+info[3]);
					petToilet2Lbl.setText("Toilet: "+info[4]);
				}else{
					actions2Lbl.setVisible(true);
				}
			}
		});
		sleep2Btn.setVisible(false);
		GridBagConstraints gbc_sleep2Btn = new GridBagConstraints();
		gbc_sleep2Btn.gridwidth = 2;
		gbc_sleep2Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_sleep2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_sleep2Btn.gridx = 1;
		gbc_sleep2Btn.gridy = 12;
		dayPnl.add(sleep2Btn, gbc_sleep2Btn);
		
		JButton sleep3Btn = new JButton("Sleep");
		sleep3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				if (player.getPets().size()>2){
					if (GameEnv.getActions3()>0){
						GameEnv.setActions3(GameEnv.getActions3()-1);
						player.getPets().get(2).sleep();
			    		petName3Lbl.setText(player.getPets().get(2).getName());
						petType3Lbl.setText(player.getPets().get(2).getClass().getSimpleName());
						String[] info = player.getPets().get(1).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);
					}else{
						actions3Lbl.setVisible(true);
					}
				}else{
					if (GameEnv.getActions2()>0){
						GameEnv.setActions2(GameEnv.getActions2()-1);
						player.getPets().get(1).sleep();
						petName3Lbl.setText(player.getPets().get(1).getName());
						petType3Lbl.setText(player.getPets().get(1).getClass().getSimpleName());
						String[] info = player.getPets().get(2).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);
					}else{
						actions3Lbl.setVisible(true);
					}
				}
				
			}
		});
		sleep3Btn.setVisible(false);
		GridBagConstraints gbc_sleep3Btn = new GridBagConstraints();
		gbc_sleep3Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_sleep3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_sleep3Btn.gridx = 3;
		gbc_sleep3Btn.gridy = 12;
		dayPnl.add(sleep3Btn, gbc_sleep3Btn);
		
		JButton toilet1Btn = new JButton("Use toilet");
		toilet1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (GameEnv.getActions1()>0){
					GameEnv.setActions1(GameEnv.getActions1()-1);
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(0).toilet();
					petName1Lbl.setText(player.getPets().get(0).getName());
					petType1Lbl.setText(player.getPets().get(0).getClass().getSimpleName());
					String[] info = player.getPets().get(0).getInfo();
					petWeight1Lbl.setText("Weight: "+info[0]);
					petHunger1Lbl.setText("Hunger: "+info[1]);
					petSickness1Lbl.setText("Sickness: "+info[2]);
					petTiredness1Lbl.setText("Tiredness: "+info[3]);
					petToilet1Lbl.setText("Toilet: "+info[4]);
				}else{
					actions1Lbl.setVisible(true);
				}
				
			}
		});
		toilet1Btn.setVisible(false);
		GridBagConstraints gbc_toilet1Btn = new GridBagConstraints();
		gbc_toilet1Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_toilet1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_toilet1Btn.gridx = 0;
		gbc_toilet1Btn.gridy = 13;
		dayPnl.add(toilet1Btn, gbc_toilet1Btn);
		
		JButton toilet2Btn = new JButton("Use toilet");
		toilet2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (GameEnv.getActions2()>0){
					GameEnv.setActions2(GameEnv.getActions2()-1);
					Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
					player.getPets().get(1).toilet();
					petName2Lbl.setText(player.getPets().get(1).getName());
					petType2Lbl.setText(player.getPets().get(1).getClass().getSimpleName());
					String[] info = player.getPets().get(1).getInfo();
					petWeight2Lbl.setText("Weight: "+info[0]);
					petHunger2Lbl.setText("Hunger: "+info[1]);
					petSickness2Lbl.setText("Sickness: "+info[2]);
					petTiredness2Lbl.setText("Tiredness: "+info[3]);
					petToilet2Lbl.setText("Toilet: "+info[4]);
				}else{
					actions2Lbl.setVisible(true);
				}
			}
		});
		toilet2Btn.setVisible(false);
		GridBagConstraints gbc_toilet2Btn = new GridBagConstraints();
		gbc_toilet2Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_toilet2Btn.gridwidth = 2;
		gbc_toilet2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_toilet2Btn.gridx = 1;
		gbc_toilet2Btn.gridy = 13;
		dayPnl.add(toilet2Btn, gbc_toilet2Btn);
		
		JButton toilet3Btn = new JButton("Use toilet");
		toilet3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pet=2;
				Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				if (player.getPets().size()<3){
					pet=1;
				}
				
				if (player.getPets().size()>2){
					if (GameEnv.getActions3()>0){
						GameEnv.setActions3(GameEnv.getActions3()-1);
						player.getPets().get(pet).toilet();
						petName3Lbl.setText(player.getPets().get(2).getName());
						petType3Lbl.setText(player.getPets().get(2).getClass().getSimpleName());
						String[] info = player.getPets().get(1).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);
					}else{
						actions3Lbl.setVisible(true);
					}
				}else{
					if (GameEnv.getActions2()>0){
						GameEnv.setActions2(GameEnv.getActions2()-1);
						player.getPets().get(pet).toilet();
						petName3Lbl.setText(player.getPets().get(1).getName());
						petType3Lbl.setText(player.getPets().get(1).getClass().getSimpleName());
						String[] info = player.getPets().get(2).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);
					}else{
						actions3Lbl.setVisible(true);
					}
				}
			}
		});
		toilet3Btn.setVisible(false);
		GridBagConstraints gbc_toilet3Btn = new GridBagConstraints();
		gbc_toilet3Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_toilet3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_toilet3Btn.gridx = 3;
		gbc_toilet3Btn.gridy = 13;
		dayPnl.add(toilet3Btn, gbc_toilet3Btn);
		
		JButton play1Btn = new JButton("Play");
		play1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				GameEnv.setCurrentPet(0);
				PlayBox playBox = new PlayBox();
				playBox.setVisible(true);
				playBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
						petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
						petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(0).getInfo();
						petWeight1Lbl.setText("Weight: "+info[0]);
						petHunger1Lbl.setText("Hunger: "+info[1]);
						petSickness1Lbl.setText("Sickness: "+info[2]);
						petTiredness1Lbl.setText("Tiredness: "+info[3]);
						petToilet1Lbl.setText("Toilet: "+info[4]);
				
				    }
				});
			}
		});
		play1Btn.setVisible(false);
		GridBagConstraints gbc_play1Btn = new GridBagConstraints();
		gbc_play1Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_play1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_play1Btn.gridx = 0;
		gbc_play1Btn.gridy = 11;
		dayPnl.add(play1Btn, gbc_play1Btn);
		
		JButton play2Btn = new JButton("Play");
		play2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				GameEnv.setCurrentPet(1);
				PlayBox playBox = new PlayBox();
				playBox.setVisible(true);
				playBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
			    		petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
						petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(1).getInfo();
						petWeight2Lbl.setText("Weight: "+info[0]);
						petHunger2Lbl.setText("Hunger: "+info[1]);
						petSickness2Lbl.setText("Sickness: "+info[2]);
						petTiredness2Lbl.setText("Tiredness: "+info[3]);
						petToilet2Lbl.setText("Toilet: "+info[4]);
						
				    }
				});
			}
		});
		play2Btn.setVisible(false);
		GridBagConstraints gbc_play2Btn = new GridBagConstraints();
		gbc_play2Btn.gridwidth = 2;
		gbc_play2Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_play2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_play2Btn.gridx = 1;
		gbc_play2Btn.gridy = 11;
		dayPnl.add(play2Btn, gbc_play2Btn);
		
		JButton play3Btn = new JButton("Play");
		play3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				if (currentPlayer.getPets().size()<3){
					GameEnv.setCurrentPet(1);
				}else{
					GameEnv.setCurrentPet(2);
				}
				
				PlayBox playBox = new PlayBox();
				playBox.setVisible(true);
				playBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
				    	
				    	if (currentPlayer.getPets().size()<3){
				    		petName3Lbl.setText(currentPlayer.getPets().get(1).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(1).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
							
						}else{
							petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(2).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
							
						}
				    }
				});
			}
		});
		play3Btn.setVisible(false);
		GridBagConstraints gbc_play3Btn = new GridBagConstraints();
		gbc_play3Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_play3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_play3Btn.gridx = 3;
		gbc_play3Btn.gridy = 11;
		dayPnl.add(play3Btn, gbc_play3Btn);
		
		JButton feed1Btn = new JButton("Feed");
		feed1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				GameEnv.setCurrentPet(0);
				FeedBox feedBox = new FeedBox();
				feedBox.setVisible(true);
				feedBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
				    	Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
						petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
						petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(0).getInfo();
						petWeight1Lbl.setText("Weight: "+info[0]);
						petHunger1Lbl.setText("Hunger: "+info[1]);
						petSickness1Lbl.setText("Sickness: "+info[2]);
						petTiredness1Lbl.setText("Tiredness: "+info[3]);
						petToilet1Lbl.setText("Toilet: "+info[4]);
						
				    }
				});
				
				/*
				petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
				petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
				info = currentPlayer.getPets().get(1).getInfo();
				petWeight2Lbl.setText("Weight: "+info[0]);
				petHunger2Lbl.setText("Hunger: "+info[1]);
				petSickness2Lbl.setText("Sickness: "+info[2]);
				petTiredness2Lbl.setText("Tiredness: "+info[3]);
				petToilet2Lbl.setText("Toilet: "+info[4]);
				actions2Lbl.setText("Actions: 2");
				petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
				petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
				info = currentPlayer.getPets().get(2).getInfo();
				petWeight3Lbl.setText("Weight: "+info[0]);
				petHunger3Lbl.setText("Hunger: "+info[1]);
				petSickness3Lbl.setText("Sickness: "+info[2]);
				petTiredness3Lbl.setText("Tiredness: "+info[3]);
				petToilet3Lbl.setText("Toilet: "+info[4]);
				actions3Lbl.setText("Actions: 2");
				*/
				//GameEnv.feed(GameEnv.getPlayers().get(GameEnv.getCurPlayer()), GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getPets().get(0));
			}
		});
		feed1Btn.setVisible(false);
		GridBagConstraints gbc_feed1Btn = new GridBagConstraints();
		gbc_feed1Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_feed1Btn.insets = new Insets(0, 0, 5, 5);
		gbc_feed1Btn.gridx = 0;
		gbc_feed1Btn.gridy = 10;
		dayPnl.add(feed1Btn, gbc_feed1Btn);
		
		JButton feed2Btn = new JButton("Feed");
		feed2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				GameEnv.setCurrentPet(1);
				FeedBox feedBox = new FeedBox();
				feedBox.setVisible(true);
				feedBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
				    	Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				    	petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
						petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(1).getInfo();
						petWeight2Lbl.setText("Weight: "+info[0]);
						petHunger2Lbl.setText("Hunger: "+info[1]);
						petSickness2Lbl.setText("Sickness: "+info[2]);
						petTiredness2Lbl.setText("Tiredness: "+info[3]);
						petToilet2Lbl.setText("Toilet: "+info[4]);
					
				    }
				});
			}
		});
		feed2Btn.setVisible(false);
		GridBagConstraints gbc_feed2Btn = new GridBagConstraints();
		gbc_feed2Btn.gridwidth = 2;
		gbc_feed2Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_feed2Btn.insets = new Insets(0, 0, 5, 5);
		gbc_feed2Btn.gridx = 1;
		gbc_feed2Btn.gridy = 10;
		dayPnl.add(feed2Btn, gbc_feed2Btn);
		
		JButton feed3Btn = new JButton("Feed");
		feed3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Component comp: dayPnl.getComponents()) {
					comp.setEnabled(false);
				}
				Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				if (currentPlayer.getPets().size()<3){
					GameEnv.setCurrentPet(1);
				}else{
					GameEnv.setCurrentPet(2);
				}
				
				FeedBox feedBox = new FeedBox();
				feedBox.setVisible(true);
				feedBox.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(true);
						}
				    	
				    	if (currentPlayer.getPets().size()<3){
				    		petName3Lbl.setText(currentPlayer.getPets().get(1).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(1).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
						
						}else{
							petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(2).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
						
						}
				    }
				});
			}
		});
		feed3Btn.setVisible(false);
		GridBagConstraints gbc_feed3Btn = new GridBagConstraints();
		gbc_feed3Btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_feed3Btn.insets = new Insets(0, 0, 5, 0);
		gbc_feed3Btn.gridx = 3;
		gbc_feed3Btn.gridy = 10;
		dayPnl.add(feed3Btn, gbc_feed3Btn);
		
		JButton nextBtn = new JButton("Finish turn");
		nextBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Pet pet : GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getPets()) {
					pet.addScore();
					pet.activityCounterReset();
					pet.endDay();
				}
				GameEnv.setActions1(2);
				GameEnv.setActions2(2);
				GameEnv.setActions3(2);
				GameEnv.getPlayers().get(GameEnv.getCurPlayer()).changeMoney(100);
				if(GameEnv.getCurPlayer()+1 == GameEnv.getPlayerNum()) {
					GameEnv.setCurPlayer(0);
					GameEnv.setCurrentDay(GameEnv.getCurrentDay()+1);
				}else{
					GameEnv.setCurPlayer(GameEnv.getCurPlayer()+1);
				}
				petName3Lbl.setVisible(false);
				petName2Lbl.setVisible(false);
				petName1Lbl.setVisible(false);
				petType1Lbl.setVisible(false);
				petType2Lbl.setVisible(false);
				petType3Lbl.setVisible(false);
				petWeight1Lbl.setVisible(false);
				petWeight2Lbl.setVisible(false);
				petWeight3Lbl.setVisible(false);
				petHunger2Lbl.setVisible(false);
				petHunger3Lbl.setVisible(false);
				petHunger1Lbl.setVisible(false);
				petSickness1Lbl.setVisible(false);
				petSickness2Lbl.setVisible(false);
				petSickness3Lbl.setVisible(false);
				petTiredness1Lbl.setVisible(false);
				petTiredness2Lbl.setVisible(false);
				petTiredness3Lbl.setVisible(false);
				petToilet2Lbl.setVisible(false);
				petToilet1Lbl.setVisible(false);
				petToilet3Lbl.setVisible(false);
				feed1Btn.setVisible(false);
				feed2Btn.setVisible(false);
				feed3Btn.setVisible(false);
				play1Btn.setVisible(false);
				play2Btn.setVisible(false);
				play3Btn.setVisible(false);
				sleep1Btn.setVisible(false);
				sleep2Btn.setVisible(false);
				sleep3Btn.setVisible(false);
				toilet2Btn.setVisible(false);
				toilet1Btn.setVisible(false);
				toilet3Btn.setVisible(false);
				actions1Lbl.setVisible(false);
				actions2Lbl.setVisible(false);
				actions3Lbl.setVisible(false);
				
				Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				playerSelectPnl.setVisible(false);
				playerPetLbl.setText(currentPlayer.getName()+", what would you like to do with your pets?");
				currentDayLbl.setText("Day "+GameEnv.getCurrentDay());
				if (currentPlayer.getPets().size() == 1) {
					petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
					petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
					String[] info = currentPlayer.getPets().get(0).getInfo();
					petWeight1Lbl.setText("Weight: "+info[0]);
					petHunger1Lbl.setText("Hunger: "+info[1]);
					petSickness1Lbl.setText("Sickness: "+info[2]);
					petTiredness1Lbl.setText("Tiredness: "+info[3]);
					petToilet1Lbl.setText("Toilet: "+info[4]);

					
					petName1Lbl.setVisible(true);
					petType1Lbl.setVisible(true);
					petWeight1Lbl.setVisible(true);
					petHunger1Lbl.setVisible(true);
					petSickness1Lbl.setVisible(true);
					petTiredness1Lbl.setVisible(true);
					petToilet1Lbl.setVisible(true);
					feed1Btn.setVisible(true);
					play1Btn.setVisible(true);
					sleep1Btn.setVisible(true);
					toilet1Btn.setVisible(true);
	
				}else if (currentPlayer.getPets().size() == 2) {
					petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
					petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
					String[] info = currentPlayer.getPets().get(0).getInfo();
					petWeight1Lbl.setText("Weight: "+info[0]);
					petHunger1Lbl.setText("Hunger: "+info[1]);
					petSickness1Lbl.setText("Sickness: "+info[2]);
					petTiredness1Lbl.setText("Tiredness: "+info[3]);
					petToilet1Lbl.setText("Toilet: "+info[4]);
				
					
					petName1Lbl.setVisible(true);
					petType1Lbl.setVisible(true);
					petWeight1Lbl.setVisible(true);
					petHunger1Lbl.setVisible(true);
					petSickness1Lbl.setVisible(true);
					petTiredness1Lbl.setVisible(true);
					petToilet1Lbl.setVisible(true);
					feed1Btn.setVisible(true);
					play1Btn.setVisible(true);
					sleep1Btn.setVisible(true);
					toilet1Btn.setVisible(true);
					
					petName3Lbl.setText(currentPlayer.getPets().get(1).getName());
					petType3Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
					info = currentPlayer.getPets().get(1).getInfo();
					petWeight3Lbl.setText("Weight: "+info[0]);
					petHunger3Lbl.setText("Hunger: "+info[1]);
					petSickness3Lbl.setText("Sickness: "+info[2]);
					petTiredness3Lbl.setText("Tiredness: "+info[3]);
					petToilet3Lbl.setText("Toilet: "+info[4]);
				
					petName3Lbl.setVisible(true);
					petType3Lbl.setVisible(true);
					petWeight3Lbl.setVisible(true);
					petHunger3Lbl.setVisible(true);
					petSickness3Lbl.setVisible(true);
					petTiredness3Lbl.setVisible(true);
					petToilet3Lbl.setVisible(true);
					feed3Btn.setVisible(true);
					play3Btn.setVisible(true);
					sleep3Btn.setVisible(true);
					toilet3Btn.setVisible(true);
				}else if (currentPlayer.getPets().size() == 3) {
					petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
					petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
					String[] info = currentPlayer.getPets().get(0).getInfo();
					petWeight1Lbl.setText("Weight: "+info[0]);
					petHunger1Lbl.setText("Hunger: "+info[1]);
					petSickness1Lbl.setText("Sickness: "+info[2]);
					petTiredness1Lbl.setText("Tiredness: "+info[3]);
					petToilet1Lbl.setText("Toilet: "+info[4]);
					
					petName1Lbl.setVisible(true);
					petType1Lbl.setVisible(true);
					petWeight1Lbl.setVisible(true);
					petHunger1Lbl.setVisible(true);
					petSickness1Lbl.setVisible(true);
					petTiredness1Lbl.setVisible(true);
					petToilet1Lbl.setVisible(true);
					feed1Btn.setVisible(true);
					play1Btn.setVisible(true);
					sleep1Btn.setVisible(true);
					toilet1Btn.setVisible(true);
					
					
					petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
					petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
					info = currentPlayer.getPets().get(1).getInfo();
					petWeight2Lbl.setText("Weight: "+info[0]);
					petHunger2Lbl.setText("Hunger: "+info[1]);
					petSickness2Lbl.setText("Sickness: "+info[2]);
					petTiredness2Lbl.setText("Tiredness: "+info[3]);
					petToilet2Lbl.setText("Toilet: "+info[4]);
					
					petName2Lbl.setVisible(true);
					petType2Lbl.setVisible(true);
					petWeight2Lbl.setVisible(true);
					petHunger2Lbl.setVisible(true);
					petSickness2Lbl.setVisible(true);
					petTiredness2Lbl.setVisible(true);
					petToilet2Lbl.setVisible(true);
					feed2Btn.setVisible(true);
					play2Btn.setVisible(true);
					sleep2Btn.setVisible(true);
					toilet2Btn.setVisible(true);
					
					petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
					petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
					info = currentPlayer.getPets().get(2).getInfo();
					petWeight3Lbl.setText("Weight: "+info[0]);
					petHunger3Lbl.setText("Hunger: "+info[1]);
					petSickness3Lbl.setText("Sickness: "+info[2]);
					petTiredness3Lbl.setText("Tiredness: "+info[3]);
					petToilet3Lbl.setText("Toilet: "+info[4]);
					
					petName3Lbl.setVisible(true);
					petType3Lbl.setVisible(true);
					petWeight3Lbl.setVisible(true);
					petHunger3Lbl.setVisible(true);
					petSickness3Lbl.setVisible(true);
					petTiredness3Lbl.setVisible(true);
					petToilet3Lbl.setVisible(true);
					feed3Btn.setVisible(true);
					play3Btn.setVisible(true);
					sleep3Btn.setVisible(true);
					toilet3Btn.setVisible(true);
				}
				System.out.println(GameEnv.getDays());
				System.out.println(GameEnv.getCurrentDay());
				if (GameEnv.getCurrentDay()>=GameEnv.getDays()+1) {
					GameEnv.setScores(GameEnv.getPlayerNum());
					ArrayList<Player> players = GameEnv.getPlayers();
					dayPnl.setVisible(false);
					String elseScores = "<html>";
					for (Player player : players) {
						elseScores += player.getName()+" : "+player.getScore()+"<br>";
					}
					elseScores+="</html>";
					elseLbl.setText(elseScores);
					endGamePnl.setVisible(true);
				}
				Player player = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
				if (GameEnv.getCurrentDay()>1&&player.getPets().size()>0) {
					String behave="None";
					Pet pett = player.getPets().get(0);
					for (Pet pet : player.getPets()){
						behave=pet.randomBehaviour();
						System.out.println(behave);
						if(behave!="None"){
							pett = pet;
							break;
						}
					}
					if (behave == "Died") {
						/*pet1Lbl.setText(pet.getName());
						pet1Lbl.setVisible(true);
						oneLbl.setText("Your pet died");
						oneLbl.setVisible(true);
						if(!pet.getDeath()){
							revive1Btn.setVisible(true);
						}*/
					}else if (behave == "Sick") {
						for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(false);
						}
						GameEnv.setPetName(pett);
						SickBox sick = new SickBox();
						sick.setVisible(true);
						sick.addWindowListener(new java.awt.event.WindowAdapter() {
						    @Override
						    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
						    	for (Component comp: dayPnl.getComponents()) {
									comp.setEnabled(true);
								}
						    }
						});
					}else if (behave == "Misbehaved") {
						for (Component comp: dayPnl.getComponents()) {
							comp.setEnabled(false);
						}
						GameEnv.setPetName(pett);
						MisbehaveBox bad = new MisbehaveBox();
						bad.setVisible(true);
						bad.addWindowListener(new java.awt.event.WindowAdapter() {
						    @Override
						    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
						    	for (Component comp: dayPnl.getComponents()) {
									comp.setEnabled(true);
								}
						    }
						});
					}
					if (currentPlayer.getPets().size() == 1) {
						petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
						petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(0).getInfo();
						petWeight1Lbl.setText("Weight: "+info[0]);
						petHunger1Lbl.setText("Hunger: "+info[1]);
						petSickness1Lbl.setText("Sickness: "+info[2]);
						petTiredness1Lbl.setText("Tiredness: "+info[3]);
						petToilet1Lbl.setText("Toilet: "+info[4]);
		
					}else if (currentPlayer.getPets().size() == 2) {
						petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
						petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(0).getInfo();
						petWeight1Lbl.setText("Weight: "+info[0]);
						petHunger1Lbl.setText("Hunger: "+info[1]);
						petSickness1Lbl.setText("Sickness: "+info[2]);
						petTiredness1Lbl.setText("Tiredness: "+info[3]);
						petToilet1Lbl.setText("Toilet: "+info[4]);
						
						petName3Lbl.setText(currentPlayer.getPets().get(1).getName());
						petType3Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
						info = currentPlayer.getPets().get(1).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);

					}else if (currentPlayer.getPets().size() == 3) {
						petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
						petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
						String[] info = currentPlayer.getPets().get(0).getInfo();
						petWeight1Lbl.setText("Weight: "+info[0]);
						petHunger1Lbl.setText("Hunger: "+info[1]);
						petSickness1Lbl.setText("Sickness: "+info[2]);
						petTiredness1Lbl.setText("Tiredness: "+info[3]);
						petToilet1Lbl.setText("Toilet: "+info[4]);

						petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
						petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
						info = currentPlayer.getPets().get(1).getInfo();
						petWeight2Lbl.setText("Weight: "+info[0]);
						petHunger2Lbl.setText("Hunger: "+info[1]);
						petSickness2Lbl.setText("Sickness: "+info[2]);
						petTiredness2Lbl.setText("Tiredness: "+info[3]);
						petToilet2Lbl.setText("Toilet: "+info[4]);
						
						petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
						petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
						info = currentPlayer.getPets().get(2).getInfo();
						petWeight3Lbl.setText("Weight: "+info[0]);
						petHunger3Lbl.setText("Hunger: "+info[1]);
						petSickness3Lbl.setText("Sickness: "+info[2]);
						petTiredness3Lbl.setText("Tiredness: "+info[3]);
						petToilet3Lbl.setText("Toilet: "+info[4]);
					}
				}
			}
		});
		GridBagConstraints gbc_nextBtn = new GridBagConstraints();
		gbc_nextBtn.insets = new Insets(0, 0, 0, 5);
		gbc_nextBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_nextBtn.gridwidth = 2;
		gbc_nextBtn.gridx = 1;
		gbc_nextBtn.gridy = 15;
		dayPnl.add(nextBtn, gbc_nextBtn);
		
		JButton btnAddPets = new JButton("Add pets");
		btnAddPets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int petNum = 0;
				if(pet1CB.getSelectedIndex()!=0) {
					petNum+=1;
				}
				if(pet2CB.getSelectedIndex()!=0) {
					petNum+=1;
				}
				if(pet3CB.getSelectedIndex()!=0) {
					petNum+=1;
				}
				if(petNum==0){
					noPetLbl.setVisible(true);	
				}else{
					ArrayList<Player> players = GameEnv.getPlayers();
					Player player = new Player(nameTF.getText(),100*petNum);
					if(pet1CB.getSelectedIndex()!=0) {
						player.addPet(GameEnv.createPet(pet1CB.getSelectedItem().toString(), pet1TF.getText()));
					}
					if(pet2CB.getSelectedIndex()!=0) {
						player.addPet(GameEnv.createPet(pet2CB.getSelectedItem().toString(), pet2TF.getText()));
					}
					if(pet3CB.getSelectedIndex()!=0) {
						player.addPet(GameEnv.createPet(pet3CB.getSelectedItem().toString(), pet3TF.getText()));
					}
					players.add(player);
					GameEnv.setPlayers(players);
					resetPlayerSelect(noPetLbl, pet1CB, pet2CB, pet3CB);
					if(GameEnv.getPlayers().size()<GameEnv.getPlayerNum()){
						playerNumLbl.setText(("Player "+(GameEnv.getPlayers().size()+1)));
					}else{
						Player currentPlayer = GameEnv.getPlayers().get(GameEnv.getCurPlayer());
						playerSelectPnl.setVisible(false);
						playerPetLbl.setText(currentPlayer.getName()+", what would you like to do with your pets?");
						currentDayLbl.setText("Day "+GameEnv.getCurrentDay());
						if (currentPlayer.getPets().size() == 1) {
							petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
							petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(0).getInfo();
							petWeight1Lbl.setText("Weight: "+info[0]);
							petHunger1Lbl.setText("Hunger: "+info[1]);
							petSickness1Lbl.setText("Sickness: "+info[2]);
							petTiredness1Lbl.setText("Tiredness: "+info[3]);
							petToilet1Lbl.setText("Toilet: "+info[4]);

							
							petName1Lbl.setVisible(true);
							petType1Lbl.setVisible(true);
							petWeight1Lbl.setVisible(true);
							petHunger1Lbl.setVisible(true);
							petSickness1Lbl.setVisible(true);
							petTiredness1Lbl.setVisible(true);
							petToilet1Lbl.setVisible(true);
							feed1Btn.setVisible(true);
							play1Btn.setVisible(true);
							sleep1Btn.setVisible(true);
							toilet1Btn.setVisible(true);
			
						}else if (currentPlayer.getPets().size() == 2) {
							petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
							petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(0).getInfo();
							petWeight1Lbl.setText("Weight: "+info[0]);
							petHunger1Lbl.setText("Hunger: "+info[1]);
							petSickness1Lbl.setText("Sickness: "+info[2]);
							petTiredness1Lbl.setText("Tiredness: "+info[3]);
							petToilet1Lbl.setText("Toilet: "+info[4]);
							
							petName1Lbl.setVisible(true);
							petType1Lbl.setVisible(true);
							petWeight1Lbl.setVisible(true);
							petHunger1Lbl.setVisible(true);
							petSickness1Lbl.setVisible(true);
							petTiredness1Lbl.setVisible(true);
							petToilet1Lbl.setVisible(true);
							feed1Btn.setVisible(true);
							play1Btn.setVisible(true);
							sleep1Btn.setVisible(true);
							toilet1Btn.setVisible(true);
							
							petName3Lbl.setText(currentPlayer.getPets().get(1).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
							info = currentPlayer.getPets().get(1).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
							
							petName3Lbl.setVisible(true);
							petType3Lbl.setVisible(true);
							petWeight3Lbl.setVisible(true);
							petHunger3Lbl.setVisible(true);
							petSickness3Lbl.setVisible(true);
							petTiredness3Lbl.setVisible(true);
							petToilet3Lbl.setVisible(true);
							feed3Btn.setVisible(true);
							play3Btn.setVisible(true);
							sleep3Btn.setVisible(true);
							toilet3Btn.setVisible(true);
						}else if (currentPlayer.getPets().size() == 3) {
							petName1Lbl.setText(currentPlayer.getPets().get(0).getName());
							petType1Lbl.setText(currentPlayer.getPets().get(0).getClass().getSimpleName());
							String[] info = currentPlayer.getPets().get(0).getInfo();
							petWeight1Lbl.setText("Weight: "+info[0]);
							petHunger1Lbl.setText("Hunger: "+info[1]);
							petSickness1Lbl.setText("Sickness: "+info[2]);
							petTiredness1Lbl.setText("Tiredness: "+info[3]);
							petToilet1Lbl.setText("Toilet: "+info[4]);
							
							petName1Lbl.setVisible(true);
							petType1Lbl.setVisible(true);
							petWeight1Lbl.setVisible(true);
							petHunger1Lbl.setVisible(true);
							petSickness1Lbl.setVisible(true);
							petTiredness1Lbl.setVisible(true);
							petToilet1Lbl.setVisible(true);
							feed1Btn.setVisible(true);
							play1Btn.setVisible(true);
							sleep1Btn.setVisible(true);
							toilet1Btn.setVisible(true);
							
							petName2Lbl.setText(currentPlayer.getPets().get(1).getName());
							petType2Lbl.setText(currentPlayer.getPets().get(1).getClass().getSimpleName());
							info = currentPlayer.getPets().get(1).getInfo();
							petWeight2Lbl.setText("Weight: "+info[0]);
							petHunger2Lbl.setText("Hunger: "+info[1]);
							petSickness2Lbl.setText("Sickness: "+info[2]);
							petTiredness2Lbl.setText("Tiredness: "+info[3]);
							petToilet2Lbl.setText("Toilet: "+info[4]);
							
							petName2Lbl.setVisible(true);
							petType2Lbl.setVisible(true);
							petWeight2Lbl.setVisible(true);
							petHunger2Lbl.setVisible(true);
							petSickness2Lbl.setVisible(true);
							petTiredness2Lbl.setVisible(true);
							petToilet2Lbl.setVisible(true);
							feed2Btn.setVisible(true);
							play2Btn.setVisible(true);
							sleep2Btn.setVisible(true);
							toilet2Btn.setVisible(true);
							
							petName3Lbl.setText(currentPlayer.getPets().get(2).getName());
							petType3Lbl.setText(currentPlayer.getPets().get(2).getClass().getSimpleName());
							info = currentPlayer.getPets().get(2).getInfo();
							petWeight3Lbl.setText("Weight: "+info[0]);
							petHunger3Lbl.setText("Hunger: "+info[1]);
							petSickness3Lbl.setText("Sickness: "+info[2]);
							petTiredness3Lbl.setText("Tiredness: "+info[3]);
							petToilet3Lbl.setText("Toilet: "+info[4]);
							
							petName3Lbl.setVisible(true);
							petType3Lbl.setVisible(true);
							petWeight3Lbl.setVisible(true);
							petHunger3Lbl.setVisible(true);
							petSickness3Lbl.setVisible(true);
							petTiredness3Lbl.setVisible(true);
							petToilet3Lbl.setVisible(true);
							feed3Btn.setVisible(true);
							play3Btn.setVisible(true);
							sleep3Btn.setVisible(true);
							toilet3Btn.setVisible(true);
						}
						
						
						dayPnl.setVisible(true);
					}
				
				}
			}
		});
		GridBagConstraints gbc_btnAddPets = new GridBagConstraints();
		gbc_btnAddPets.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddPets.gridx = 1;
		gbc_btnAddPets.gridy = 11;
		playerSelectPnl.add(btnAddPets, gbc_btnAddPets);
		
		JPanel leaderBoardPnl = new JPanel();
		cardPnl.add(leaderBoardPnl, "name_3235618843376452");
		GridBagLayout gbl_leaderBoardPnl = new GridBagLayout();
		gbl_leaderBoardPnl.columnWidths = new int[]{0, 0, 0, 0};
		gbl_leaderBoardPnl.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_leaderBoardPnl.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_leaderBoardPnl.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		leaderBoardPnl.setLayout(gbl_leaderBoardPnl);
		
		JLabel lblOnlineLeaderBoard = new JLabel("Online leader Board");
		lblOnlineLeaderBoard.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblOnlineLeaderBoard = new GridBagConstraints();
		gbc_lblOnlineLeaderBoard.gridwidth = 3;
		gbc_lblOnlineLeaderBoard.insets = new Insets(0, 0, 5, 0);
		gbc_lblOnlineLeaderBoard.fill = GridBagConstraints.VERTICAL;
		gbc_lblOnlineLeaderBoard.gridx = 0;
		gbc_lblOnlineLeaderBoard.gridy = 0;
		leaderBoardPnl.add(lblOnlineLeaderBoard, gbc_lblOnlineLeaderBoard);
		
		JLabel rankLbl = new JLabel("rank");
		GridBagConstraints gbc_rankLbl = new GridBagConstraints();
		gbc_rankLbl.anchor = GridBagConstraints.EAST;
		gbc_rankLbl.insets = new Insets(0, 0, 5, 5);
		gbc_rankLbl.gridx = 0;
		gbc_rankLbl.gridy = 1;
		leaderBoardPnl.add(rankLbl, gbc_rankLbl);
		
		JLabel nameScoreLbl = new JLabel("Name");
		GridBagConstraints gbc_nameScoreLbl = new GridBagConstraints();
		gbc_nameScoreLbl.insets = new Insets(0, 0, 5, 5);
		gbc_nameScoreLbl.gridx = 1;
		gbc_nameScoreLbl.gridy = 1;
		leaderBoardPnl.add(nameScoreLbl, gbc_nameScoreLbl);
		
		JLabel lblScore = new JLabel("Score");
		GridBagConstraints gbc_lblScore = new GridBagConstraints();
		gbc_lblScore.insets = new Insets(0, 0, 5, 0);
		gbc_lblScore.anchor = GridBagConstraints.WEST;
		gbc_lblScore.gridx = 2;
		gbc_lblScore.gridy = 1;
		leaderBoardPnl.add(lblScore, gbc_lblScore);
		
		JLabel label1 = new JLabel("1");
		GridBagConstraints gbc_label1 = new GridBagConstraints();
		gbc_label1.anchor = GridBagConstraints.EAST;
		gbc_label1.insets = new Insets(0, 0, 5, 5);
		gbc_label1.gridx = 0;
		gbc_label1.gridy = 2;
		leaderBoardPnl.add(label1, gbc_label1);
		
		JLabel name1Lbl = new JLabel("New label");
		GridBagConstraints gbc_name1Lbl = new GridBagConstraints();
		gbc_name1Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name1Lbl.gridx = 1;
		gbc_name1Lbl.gridy = 2;
		leaderBoardPnl.add(name1Lbl, gbc_name1Lbl);
		
		JLabel score1Lbl = new JLabel("New label");
		GridBagConstraints gbc_score1Lbl = new GridBagConstraints();
		gbc_score1Lbl.anchor = GridBagConstraints.WEST;
		gbc_score1Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score1Lbl.gridx = 2;
		gbc_score1Lbl.gridy = 2;
		leaderBoardPnl.add(score1Lbl, gbc_score1Lbl);
		
		JLabel label_1 = new JLabel("2");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 3;
		leaderBoardPnl.add(label_1, gbc_label_1);
		
		JLabel name2Lbl = new JLabel("New label");
		GridBagConstraints gbc_name2Lbl = new GridBagConstraints();
		gbc_name2Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name2Lbl.gridx = 1;
		gbc_name2Lbl.gridy = 3;
		leaderBoardPnl.add(name2Lbl, gbc_name2Lbl);
		
		JLabel score2Lbl = new JLabel("New label");
		GridBagConstraints gbc_score2Lbl = new GridBagConstraints();
		gbc_score2Lbl.anchor = GridBagConstraints.WEST;
		gbc_score2Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score2Lbl.gridx = 2;
		gbc_score2Lbl.gridy = 3;
		leaderBoardPnl.add(score2Lbl, gbc_score2Lbl);
		
		JLabel label_2 = new JLabel("3");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.EAST;
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 4;
		leaderBoardPnl.add(label_2, gbc_label_2);
		
		JLabel name3Lbl = new JLabel("New label");
		GridBagConstraints gbc_name3Lbl = new GridBagConstraints();
		gbc_name3Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name3Lbl.gridx = 1;
		gbc_name3Lbl.gridy = 4;
		leaderBoardPnl.add(name3Lbl, gbc_name3Lbl);
		
		JLabel score3Lbl = new JLabel("New label");
		GridBagConstraints gbc_score3Lbl = new GridBagConstraints();
		gbc_score3Lbl.anchor = GridBagConstraints.WEST;
		gbc_score3Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score3Lbl.gridx = 2;
		gbc_score3Lbl.gridy = 4;
		leaderBoardPnl.add(score3Lbl, gbc_score3Lbl);
		
		JLabel label_3 = new JLabel("4");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.EAST;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 0;
		gbc_label_3.gridy = 5;
		leaderBoardPnl.add(label_3, gbc_label_3);
		
		JLabel name4Lbl = new JLabel("New label");
		GridBagConstraints gbc_name4Lbl = new GridBagConstraints();
		gbc_name4Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name4Lbl.gridx = 1;
		gbc_name4Lbl.gridy = 5;
		leaderBoardPnl.add(name4Lbl, gbc_name4Lbl);
		
		JLabel score4Lbl = new JLabel("New label");
		GridBagConstraints gbc_score4Lbl = new GridBagConstraints();
		gbc_score4Lbl.anchor = GridBagConstraints.WEST;
		gbc_score4Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score4Lbl.gridx = 2;
		gbc_score4Lbl.gridy = 5;
		leaderBoardPnl.add(score4Lbl, gbc_score4Lbl);
		
		JLabel label_4 = new JLabel("5");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.EAST;
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 0;
		gbc_label_4.gridy = 6;
		leaderBoardPnl.add(label_4, gbc_label_4);
		
		JLabel name5Lbl = new JLabel("New label");
		GridBagConstraints gbc_name5Lbl = new GridBagConstraints();
		gbc_name5Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name5Lbl.gridx = 1;
		gbc_name5Lbl.gridy = 6;
		leaderBoardPnl.add(name5Lbl, gbc_name5Lbl);
		
		JLabel score5Lbl = new JLabel("New label");
		GridBagConstraints gbc_score5Lbl = new GridBagConstraints();
		gbc_score5Lbl.anchor = GridBagConstraints.WEST;
		gbc_score5Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score5Lbl.gridx = 2;
		gbc_score5Lbl.gridy = 6;
		leaderBoardPnl.add(score5Lbl, gbc_score5Lbl);
		
		JLabel label_5 = new JLabel("6");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.EAST;
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 0;
		gbc_label_5.gridy = 7;
		leaderBoardPnl.add(label_5, gbc_label_5);
		
		JLabel name6Lbl = new JLabel("New label");
		GridBagConstraints gbc_name6Lbl = new GridBagConstraints();
		gbc_name6Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name6Lbl.gridx = 1;
		gbc_name6Lbl.gridy = 7;
		leaderBoardPnl.add(name6Lbl, gbc_name6Lbl);
		
		JLabel score6Lbl = new JLabel("New label");
		GridBagConstraints gbc_score6Lbl = new GridBagConstraints();
		gbc_score6Lbl.anchor = GridBagConstraints.WEST;
		gbc_score6Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score6Lbl.gridx = 2;
		gbc_score6Lbl.gridy = 7;
		leaderBoardPnl.add(score6Lbl, gbc_score6Lbl);
		
		JLabel label_6 = new JLabel("7");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.EAST;
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 0;
		gbc_label_6.gridy = 8;
		leaderBoardPnl.add(label_6, gbc_label_6);
		
		JLabel name7Lbl = new JLabel("New label");
		GridBagConstraints gbc_name7Lbl = new GridBagConstraints();
		gbc_name7Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name7Lbl.gridx = 1;
		gbc_name7Lbl.gridy = 8;
		leaderBoardPnl.add(name7Lbl, gbc_name7Lbl);
		
		JLabel score7Lbl = new JLabel("New label");
		GridBagConstraints gbc_score7Lbl = new GridBagConstraints();
		gbc_score7Lbl.anchor = GridBagConstraints.WEST;
		gbc_score7Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score7Lbl.gridx = 2;
		gbc_score7Lbl.gridy = 8;
		leaderBoardPnl.add(score7Lbl, gbc_score7Lbl);
		
		JLabel label_7 = new JLabel("8");
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.EAST;
		gbc_label_7.insets = new Insets(0, 0, 5, 5);
		gbc_label_7.gridx = 0;
		gbc_label_7.gridy = 9;
		leaderBoardPnl.add(label_7, gbc_label_7);
		
		JLabel name8Lbl = new JLabel("New label");
		GridBagConstraints gbc_name8Lbl = new GridBagConstraints();
		gbc_name8Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name8Lbl.gridx = 1;
		gbc_name8Lbl.gridy = 9;
		leaderBoardPnl.add(name8Lbl, gbc_name8Lbl);
		
		JLabel score8Lbl = new JLabel("New label");
		GridBagConstraints gbc_score8Lbl = new GridBagConstraints();
		gbc_score8Lbl.anchor = GridBagConstraints.WEST;
		gbc_score8Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score8Lbl.gridx = 2;
		gbc_score8Lbl.gridy = 9;
		leaderBoardPnl.add(score8Lbl, gbc_score8Lbl);
		
		JLabel label_8 = new JLabel("9");
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.EAST;
		gbc_label_8.insets = new Insets(0, 0, 5, 5);
		gbc_label_8.gridx = 0;
		gbc_label_8.gridy = 10;
		leaderBoardPnl.add(label_8, gbc_label_8);
		
		JLabel name9Lbl = new JLabel("New label");
		GridBagConstraints gbc_name9Lbl = new GridBagConstraints();
		gbc_name9Lbl.insets = new Insets(0, 0, 5, 5);
		gbc_name9Lbl.gridx = 1;
		gbc_name9Lbl.gridy = 10;
		leaderBoardPnl.add(name9Lbl, gbc_name9Lbl);
		
		JLabel score9Lbl = new JLabel("New label");
		GridBagConstraints gbc_score9Lbl = new GridBagConstraints();
		gbc_score9Lbl.anchor = GridBagConstraints.WEST;
		gbc_score9Lbl.insets = new Insets(0, 0, 5, 0);
		gbc_score9Lbl.gridx = 2;
		gbc_score9Lbl.gridy = 10;
		leaderBoardPnl.add(score9Lbl, gbc_score9Lbl);
		
		JLabel label_9 = new JLabel("10");
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.anchor = GridBagConstraints.EAST;
		gbc_label_9.insets = new Insets(0, 0, 0, 5);
		gbc_label_9.gridx = 0;
		gbc_label_9.gridy = 11;
		leaderBoardPnl.add(label_9, gbc_label_9);
		
		JLabel name10Lbl = new JLabel("New label");
		GridBagConstraints gbc_name10Lbl = new GridBagConstraints();
		gbc_name10Lbl.insets = new Insets(0, 0, 0, 5);
		gbc_name10Lbl.gridx = 1;
		gbc_name10Lbl.gridy = 11;
		leaderBoardPnl.add(name10Lbl, gbc_name10Lbl);
		
		JLabel score10Lbl = new JLabel("New label");
		GridBagConstraints gbc_score10Lbl = new GridBagConstraints();
		gbc_score10Lbl.anchor = GridBagConstraints.WEST;
		gbc_score10Lbl.gridx = 2;
		gbc_score10Lbl.gridy = 11;
		leaderBoardPnl.add(score10Lbl, gbc_score10Lbl);
		
		JButton viewLeaderBtn = new JButton("View Leader Board");
		viewLeaderBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JSONObject json = null;
				try {
					json = JsonReader.readJsonFromUrl("http://139.59.122.88/get_scores/");
				} catch (IOException | JSONException e1) {
					e1.printStackTrace();
				}
				try {
					String[] score1 = json.get("score1").toString().split(",");
					name1Lbl.setText(score1[1]);
					score1Lbl.setText(score1[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score2 = json.get("score2").toString().split(",");
					name2Lbl.setText(score2[1]);
					score2Lbl.setText(score2[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score3 = json.get("score3").toString().split(",");
					name3Lbl.setText(score3[1]);
					score3Lbl.setText(score3[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score4 = json.get("score4").toString().split(",");
					name4Lbl.setText(score4[1]);
					score4Lbl.setText(score4[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score5 = json.get("score5").toString().split(",");
					name5Lbl.setText(score5[1]);
					score5Lbl.setText(score5[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score6 = json.get("score6").toString().split(",");
					name6Lbl.setText(score6[1]);
					score6Lbl.setText(score6[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score7 = json.get("score7").toString().split(",");
					name7Lbl.setText(score7[1]);
					score7Lbl.setText(score7[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score8 = json.get("score8").toString().split(",");
					name8Lbl.setText(score8[1]);
					score8Lbl.setText(score8[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score9 = json.get("score9").toString().split(",");
					name9Lbl.setText(score9[1]);
					score9Lbl.setText(score9[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score10 = json.get("score10").toString().split(",");
					name10Lbl.setText(score10[1]);
					score10Lbl.setText(score10[0]);
				} catch (JSONException e1) {
				}
				homePnl.setVisible(false);
				leaderBoardPnl.setVisible(true);
			}
		});
		GridBagConstraints gbc_viewLeaderBtn = new GridBagConstraints();
		gbc_viewLeaderBtn.fill = GridBagConstraints.VERTICAL;
		gbc_viewLeaderBtn.insets = new Insets(0, 0, 5, 0);
		gbc_viewLeaderBtn.gridx = 0;
		gbc_viewLeaderBtn.gridy = 3;
		homePnl.add(viewLeaderBtn, gbc_viewLeaderBtn);
		
		JButton btnUploadScores = new JButton("Upload Scores");
		btnUploadScores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Player player : GameEnv.getPlayers()) {
					URLCon.request("http://139.59.122.88/add_score/"+player.getName()+"/"+player.getScore()); 
				}
				JSONObject json = null;
				try {
					json = JsonReader.readJsonFromUrl("http://139.59.122.88/get_scores/");
				} catch (IOException | JSONException e1) {
					e1.printStackTrace();
				}
				try {
					String[] score1 = json.get("score1").toString().split(",");
					name1Lbl.setText(score1[1]);
					score1Lbl.setText(score1[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score2 = json.get("score2").toString().split(",");
					name2Lbl.setText(score2[1]);
					score2Lbl.setText(score2[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score3 = json.get("score3").toString().split(",");
					name3Lbl.setText(score3[1]);
					score3Lbl.setText(score3[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score4 = json.get("score4").toString().split(",");
					name4Lbl.setText(score4[1]);
					score4Lbl.setText(score4[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score5 = json.get("score5").toString().split(",");
					name5Lbl.setText(score5[1]);
					score5Lbl.setText(score5[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score6 = json.get("score6").toString().split(",");
					name6Lbl.setText(score6[1]);
					score6Lbl.setText(score6[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score7 = json.get("score7").toString().split(",");
					name7Lbl.setText(score7[1]);
					score7Lbl.setText(score7[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score8 = json.get("score8").toString().split(",");
					name8Lbl.setText(score8[1]);
					score8Lbl.setText(score8[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score9 = json.get("score9").toString().split(",");
					name9Lbl.setText(score9[1]);
					score9Lbl.setText(score9[0]);
				} catch (JSONException e1) {
				}
				try {
					String[] score10 = json.get("score10").toString().split(",");
					name10Lbl.setText(score10[1]);
					score10Lbl.setText(score10[0]);
				} catch (JSONException e1) {
				}
				endGamePnl.setVisible(false);
				leaderBoardPnl.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnUploadScores = new GridBagConstraints();
		gbc_btnUploadScores.gridx = 0;
		gbc_btnUploadScores.gridy = 2;
		endGamePnl.add(btnUploadScores, gbc_btnUploadScores);
	}
	
	
	
	public void resetPlayerSelect(JLabel noPetLbl, JComboBox pet1CB, JComboBox pet2CB, JComboBox pet3CB) {
		nameTF.setText("");
		pet1TF.setText("Pet1");
		pet2TF.setText("Pet2");
		pet3TF.setText("Pet3");
		pet1CB.setSelectedIndex(0);
		pet2CB.setSelectedIndex(0);
		pet3CB.setSelectedIndex(0);
		noPetLbl.setVisible(false);
	}
}
