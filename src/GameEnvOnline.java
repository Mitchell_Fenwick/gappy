


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;




public class GameEnvOnline {
	public static int pin;
	public static Scanner scan = new Scanner(System.in);
	
	public static void setScoresOnline(Player player) {
		ArrayList<Pet> currPets = player.getPets();
		double playerScore = 0;
		for(Pet pet : currPets) {
			playerScore += pet.getScore()/GameEnv.days;
		}
		player.setScore(playerScore/currPets.size());

	}
	
	public static void run(int pin) {
        String players = "";
        boolean keepRunning = true;
        while (keepRunning) {
        	String players2 = URLCon.request("http://139.59.122.88/players/"+pin);
        	if(!players2.equals(players)) {
        		String[] parts = players.split(" ");
        		String[] parts2 = players2.split(" ");
        		for (int i =1;i<=parts2.length-parts.length;i++) {
        			System.out.println(parts2[parts2.length-i]);
        		}
        		players = players2;
        	}
        	keepRunning = Boolean.parseBoolean(URLCon.request("http://139.59.122.88/ready/"+pin));
        	
        	try {Thread.sleep(1000);} catch (InterruptedException e) {}
        }
        System.out.println("ready with");
    }
	
	
	public static String createOrJoin() {
		System.out.println("1) Create new game");
		System.out.println("2) Join game");
		String playerName = null;
		int option = Integer.parseInt(scan.nextLine().trim());
		if (option == 1) {
			playerName = GameEnv.playerName("Please enter a Username(3-20 characters)", true);
			System.out.println("Please enter the number of days you would like your game to run");
			GameEnv.days = Integer.parseInt(scan.nextLine().trim());
			pin = Integer.parseInt(URLCon.request("http://139.59.122.88/create_pin/"+playerName+"/"+GameEnv.days));
			SecondLoop loop = new SecondLoop(pin);
			Thread t = new Thread(loop);
	        t.start();
	        Scanner s = new Scanner(System.in);
	        while (!s.next().equals("ready"));
	        loop.keepRunning = false;
	        t.interrupt();
	        URLCon.request("http://139.59.122.88/ready_up/"+pin+"/"+playerName);
		}else if (option==2) {
			System.out.println("Enter the game pin");
			while (true) {
				pin = Integer.parseInt(scan.nextLine().trim());
				boolean bool = Boolean.parseBoolean(URLCon.request("http://139.59.122.88/game/"+pin));
				if (bool) {
					break;
				}
				System.out.println("That is not a valid game pin try again");
			}
			while (true) {
				playerName = GameEnv.playerName("Please enter a Username(3-20 characters)", true);
				String players = URLCon.request("http://139.59.122.88/players/"+pin);
				String[] parts = players.split(" ");
				if (Arrays.asList(parts).contains(playerName)) {
					System.out.println("That username is taken by somone else in that game");
				}else{
					break;
				}
			}
			GameEnv.days = Integer.parseInt(URLCon.request("http://139.59.122.88/add_player/"+playerName+"/"+pin));
			System.out.println("Waiting for other players to join...");
			run(pin);	
		}
		return playerName;
	}
	
	public static void readyUp(String playerName, String str) {
		URLCon.request("http://139.59.122.88/ready_up/"+pin+"/"+playerName);
		if (Boolean.parseBoolean(URLCon.request("http://139.59.122.88/ready/"+pin))) {
			System.out.println(str);
			System.out.println("Ready:");
			boolean keepRunning = true;
			String players1 = "";
			while (keepRunning) {
				String players2 = URLCon.request("http://139.59.122.88/ready_players/"+pin);
	        	if(!players2.equals(players1)) {
	        		String[] parts = players1.split(" ");
	        		String[] parts2 = players2.split(" ");
	        		for (int i =1;i<=parts2.length-parts.length;i++) {
	        			System.out.println(parts2[parts2.length-i]);
	        		}
	        		players1 = players2;
	        	}
				keepRunning = Boolean.parseBoolean(URLCon.request("http://139.59.122.88/ready/"+pin));
				try {Thread.sleep(500);} catch (InterruptedException e) {}
			}	
		}
		System.out.println("All players are ready");
	}
	
	public static void online() {
		String playerName = createOrJoin();
		try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		URLCon.request("http://139.59.122.88/unready/"+pin+"/"+playerName);
		System.out.println("How many pets would you like(1-3)");
		int petsNum = Integer.parseInt(scan.nextLine().trim());
		Player player = new Player(playerName, 100*petsNum);
		for(int j = 1; j<petsNum+1;j++) {
			String jpet = GameEnv.choosePet(j);
			String petName = GameEnv.inputName("please enter the name of your new " + jpet + " (3-20 characters)", false, player.getPets());
			player.addPet(GameEnv.createPet(jpet, petName));
		}
		
		readyUp(playerName, "Waiting for other players to select there pets");
	    /* main loop */
		for(GameEnv.currentDay=1; GameEnv.currentDay< GameEnv.days+1; GameEnv.currentDay++) {
			try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
			URLCon.request("http://139.59.122.88/unready/"+pin+"/"+playerName);
			System.out.println("-------------------------");
			System.out.println("Day "+ GameEnv.currentDay);
			System.out.println("-------------------------");
			ArrayList<Pet> currPets = player.getPets();
			for(int k=0; k<currPets.size();k++) {
				Pet currPet = currPets.get(k);
				if (!currPet.getAlive()){
					continue;
				}
				while (currPet.getActivityCounter()<2){
					if ( GameEnv.currentDay > 1) {
						player.changeMoney( GameEnv.dealWithRandomBehav(currPet.randomBehaviour(), currPet));
					}
					if (currPet.getActivityCounter()<2){
						 GameEnv.doOption(currPet, player);
					}
				}
				currPet.addScore();
				currPet.activityCounterReset();
				currPet.endDay();
			}
			readyUp(playerName, "Waiting for other players to finish there day");
			player.changeMoney(100*petsNum);
		}
		/* end game */
		System.out.println("\n\n\nGame Scores (average daily score)");
		setScoresOnline(player);
		URLCon.request("http://139.59.122.88/add_score/"+pin+"/"+playerName+"/"+(int) player.getScore());
		try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
		String players = URLCon.request("http://139.59.122.88/get_scores/"+pin);
		String[] parts = players.split(" ");
		String winner = "placeHolder-0";
		for (String part : parts) {
			if (Integer.parseInt(winner.split("-")[1]) < Integer.parseInt(part.split("-")[1])) {
				winner = part;
			}
		}
		
		System.out.println("Winner: "+ winner.split("-")[0]);
		System.out.println("------------------------");
		for (String part : parts) {
			System.out.println(part.split("-")[0] + ": " + part.split("-")[1]);
		}
		
	}
	
	
	
}
