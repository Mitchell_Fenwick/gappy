import java.util.*;

public class Player {
	private String name;
	private ArrayList<Pet> pets = new ArrayList<Pet>();
	private ArrayList<Food> food = new ArrayList<Food>();
	private ArrayList<Toy> toys = new ArrayList<Toy>();
	private double money;
	private double score;
	
	/**
	 * Constructor
	 * @param playerName
	 * @param numberOfPets
	 * @param amountOfMoney
	 */
	public Player(String playerName,  double amountOfMoney){
		name = playerName;

		money = amountOfMoney;	
	}
	
	/**
	 * @return money
	 */
	public double getMoney() {
		return money;
	}
	
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * adds pet to pets array
	 * @param newPet
	 */
	public void addPet(Pet newPet){
		pets.add(newPet);
	}
	
	/**
	 * adds new toy to toys array
	 * @param newToy
	 */
	public void addToy(Toy newToy) {
		toys.add(newToy);
	}
	
	/**
	 * adds new Food to food array
	 * @param newFood
	 */
	public void addFood(Food newFood) {
		food.add(newFood);
	}
	
	/**
	 * adds or subtracts money from money attribute
	 * @param newMoney
	 */
	public void changeMoney(double newMoney) {
		money += newMoney;
	}
	
	/**
	 * removes toy from toys array
	 * @param oldToy
	 */
	public void removeToy(Toy oldToy) {
		toys.remove(oldToy);
	}
	
	/**
	 * removes food from food Array
	 * @param oldFood
	 */
	public void removeFood(Food oldFood) {
		food.remove(oldFood);
	}
	
	/**
	 * returns pets
	 * @return
	 */
	public ArrayList<Pet> getPets(){
		return pets;
	}
	
	/**
	 * returns food
	 * @return
	 */
	public ArrayList<Food> getFood(){
		return food;
	}
	
	/**
	 * returns toys
	 * @return
	 */
	public ArrayList<Toy> getToys(){
		return toys;
	}

	/**
	 * returns score
	 * @return
	 */
	public double getScore() {
		return score;
	}

	/**
	 * sets score
	 * @param score
	 */
	public void setScore(double score) {
		this.score = score;
	}
	
	public static class Comparators {
		public static final Comparator<Player> SCORE = (Player o1, Player o2) -> Double.compare(o1.score, o2.score);
	}
}
