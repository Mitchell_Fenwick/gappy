

import java.util.*;


public class GameEnv {
	private static ArrayList<Player> players = new ArrayList<Player>();
	private static int days;
	private static int playerNum;
	private static int currentDay = 1;
	private static int petNum;
	private static int currentPlayer;
	private static int currentPet;
	private static int actions1=2;
	private static int actions2=2;
	private static int actions3=2;
	private static Pet petName;
	private static Scanner scan = new Scanner(System.in);
	private static ArrayList<Pet> pets = new ArrayList<Pet>();
	
	
	/**
	 * adds pets to pets list
	 */
	public static void petsInitialize() {
		pets.add(new Dog("test"));
		pets.add(new Cat("test"));
		pets.add(new Chinchilla("test"));
		pets.add(new Snake("test"));
		pets.add(new Bear("test"));
		pets.add(new Tiger("test"));
	
	}
	
	/**
	 * error checks name input
	 * @param str
	 * @param player
	 * @param currPlayer
	 * @return
	 */
	public static String inputName(String str, boolean player, ArrayList<Pet> pets){
		String inputName;
		while(true){
			do {
				System.out.println(str);
				while(!scan.hasNextLine()){
					System.out.println(str);
					scan.next();
				}
				inputName = scan.nextLine();
				if(inputName.length() < 3 || inputName.length() > 20){
					System.out.println("Player name must be between 3 and 20 characters");
				}
	
		    } while(inputName.length() < 3 || inputName.length() > 20);
			
			boolean used = false;
			
			if (player){
				for(Player obj : players){
					String currName = obj.getName();
					if(currName.equals(inputName)) {
						System.out.println("Another player is already using that name");
						used = true;
					}
				}
			}else{
				for(Pet obj : pets){
					String currName = obj.getName();
					if(currName.equals(inputName)) {
						System.out.println("You already have a pet with that name");
						used = true;
					}
				}
			}
			if (!used) {
				break;
			}
		}
		return inputName;
		
	}
	
	/**
	 * redirects without a useless parameter
	 * @param str
	 * @param player
	 * @return
	 */
	public static String playerName(String str, boolean player){
		return inputName(str, player, new ArrayList<Pet>());
	}
	
	/**
	 * Creates available pet Menu
	 * @param j
	 * @return
	 */
	public static String choosePet(int j) {
		while(true){
			System.out.println("Choose your pet #"+j);
			System.out.println("type pet number to view");
			int k=1;
			for (Pet pet: pets){
				System.out.println(k+") "+ pet.getClass().getSimpleName());
				k++;
			}
			int viewPet = Integer.parseInt(scan.nextLine().trim());
			System.out.println(pets.get(viewPet-1).getStats());
			System.out.println("1) to confirm this pet");
			System.out.println("2) to go back");
			int selectPet = Integer.parseInt(scan.nextLine().trim());
			if (selectPet==1){
				return pets.get(viewPet-1).getClass().getSimpleName();
			}
		}
	}
	
	/**
	 * Creates pet object
	 * @param petType
	 * @param petName
	 * @return
	 */
	/**
	 * Creates pet object
	 * @param petType
	 * @param petName
	 * @return
	 */
	public static Pet createPet(String petType, String petName) {
		System.out.println(petType);
		if (petType.equals("Dog")) {
			return new Dog(petName);
		}else if (petType.equals("Cat")){
			return new Cat(petName);
		}else if (petType.equals("Snake")){
			return new Snake(petName);
		}else if (petType.equals("Chinchilla")){
			return new Chinchilla(petName);
		}else if (petType.equals("Tiger")){
			return new Tiger(petName);
		}else if (petType.equals("Bear")){
			return new Bear(petName);
		}
		throw new IllegalArgumentException();
		
	}
	
	/**
	 * feeds pet
	 * @param player
	 * @param currPet
	 * @return
	 */
	public static boolean feed(Player player, Pet currPet) {
		if (player.getFood().size()>0){
			int i = 1;
			System.out.println("Please select the type of food");
			for (Food foodItem : player.getFood()) {
				System.out.println(i+") "+foodItem.getClass().getSimpleName());
				i++;
			}
			int foodNum = Integer.parseInt(scan.nextLine().trim());
			currPet.feed(player.getFood().get(foodNum-1));
			player.getFood().remove(foodNum-1);
			System.out.println("-------------------------");
			System.out.println(currPet.getInfo());
			System.out.println("-------------------------");
			return true;
		}else{ 
			System.out.println("Sorry you have no food, visit the store to purchase sum");
			return false;
		}
	}
	
	/**
	 * plays with pet
	 * @param player
	 * @param currPet
	 * @return
	 */
	public static boolean play(Player player, Pet currPet) {
		if (player.getToys().size()>0){
			int i = 1;
			System.out.println("Please select the Toy to play with");
			for (Toy toyItem : player.getToys()) {
				System.out.println(i+") "+toyItem.getClass().getSimpleName());
				i++;
			}
			int toyNum = Integer.parseInt(scan.nextLine().trim());
			currPet.play(player.getToys().get(toyNum-1));
			if (player.getToys().get(toyNum-1).getDuribility()<1){
				player.getToys().remove(toyNum-1);
			}
			System.out.println("-------------------------");
			System.out.println(currPet.getInfo());
			System.out.println("-------------------------");
			return true;
		}else{ 
			System.out.println("Sorry you have no Toys, visit the store to purchase sum");
			return false;
		}
	}
	
	/**
	 * allows access to store
	 * @param player
	 */
	public static void openStore(Player player) {
		System.out.println("Welcome to the store");
		while(true) {
			System.out.println("Your account balance is: $"+player.getMoney());
			System.out.println("1) Purchase a Toy");
			System.out.println("2) purchase Food");
			System.out.println("3) Exit store");
			int storeOption = Integer.parseInt(scan.nextLine().trim());
			if (storeOption == 1) {
				System.out.println("Your account balance is: $"+player.getMoney());
				System.out.println("1) return to store home");
				int i = 2;
				for(Toy toy : Store.getToys()) {
					System.out.println(i + ") "+toy.getClass().getSimpleName()+ " $"+toy.getPrice());
					i++;
				}
				int toyOption = Integer.parseInt(scan.nextLine().trim());
				if(toyOption == 1){
				}else if (Store.getToys().get(toyOption-2).getPrice() <= player.getMoney()) {
					Store.buyToy(player, Store.getToys().get(toyOption-2));
					System.out.println("Purchase successful, " + (Store.getToys().get(toyOption-2)).getClass().getSimpleName() + " added to your inventory");
				}else{
					System.out.println("Sorry you dont have enough money for this toy");
				}
			}else if (storeOption == 2) {
				System.out.println("Your account balance is: $"+player.getMoney());
				System.out.println("1) return to store home");
				int i = 2;
				for(Food food : Store.getFood()) {
					System.out.println(i + ") "+food.getClass().getSimpleName()+ " $"+food.getPrice());
					i++;
				}
				int foodOption = Integer.parseInt(scan.nextLine().trim());
				if (foodOption==1){
				}else if (Store.getFood().get(foodOption-2).getPrice() <= player.getMoney()) {
					Store.buyFood(player, Store.getFood().get(foodOption-2));
					System.out.println("Purchase successful, " + (Store.getFood().get(foodOption-2)).getClass().getSimpleName() + " added to your inventory");
				}else{
					System.out.println("Sorry you dont have enough money for this Food");
				}
			}else{
				break;
			}
		}
	}
	
	/**
	 * gives main menu options
	 * @param currPet
	 * @return
	 */
	public static int getOption(Pet currPet) {
		System.out.println("What would you like to do with your "+currPet.getClass().getSimpleName()+", "+currPet.getName());
		System.out.println("1) Show Info");
		System.out.println("2) Visit the store");
		System.out.println("3) Feed");
		System.out.println("4) Play");
		System.out.println("5) Sleep");
		System.out.println("6) Toilet");
		System.out.println("7) Show inventory");
		System.out.println("8) Move to next day");
		return Integer.parseInt(scan.nextLine().trim());
	}
	
	/**
	 * shows players inventory
	 * @param player
	 */
	public static void printInv(Player player) {
		System.out.println("-------------------------");
		System.out.println("Food:");
		for (Food food : player.getFood()) {
			System.out.println(food.getClass().getSimpleName());
		}
		System.out.println("-------------------------");
		System.out.println("Toys:");
		for (Toy toy : player.getToys()) {
			System.out.println(toy.getClass().getSimpleName());
		}
		System.out.println("-------------------------");
	}
	
	/**
	 * action based on menu selection
	 * @param currPet
	 * @param player
	 */
	public static void doOption(Pet currPet, Player player) {
		while(true) {
			int option = getOption(currPet);
			if (option == 1) {
				System.out.println("-------------------------");
				System.out.println(currPet.getInfo());
				System.out.println("-------------------------");
			}else if (option == 2){
				openStore(player);
			}else if (option == 3){
				if (feed(player, currPet)) {
					break;
				}
			}else if (option == 4){
				if (play(player, currPet)) {
					break;
				}
			}else if (option == 5){
				currPet.sleep();
				System.out.println("-------------------------");
				System.out.println(currPet.getInfo());
				System.out.println("-------------------------");
				break;
			}else if (option == 6){
				currPet.toilet();
				System.out.println("-------------------------");
				System.out.println(currPet.getInfo());
				System.out.println("-------------------------");
				break;
			}else if (option == 7){
				printInv(player);
			}else if (option == 8){
				currPet.incActivityCounter();
				break;
			}
		}
		currPet.incActivityCounter();
	}
	
	/**
	 * sets scores for that day
	 * @param playersNum
	 */
	public static void setScores(int playersNum) {
		for(int i=1; i <playersNum+1; i++) {
			Player currPlayer = players.get(i-1);
			ArrayList<Pet> currPets = currPlayer.getPets();
			double playerScore = 0;
			for(Pet pet : currPets) {
				playerScore += pet.getScore()/days;
			}
			currPlayer.setScore(playerScore/currPets.size());
		}
	}
	
	/**
	 * deals with case of death
	 * @param pet
	 */
	public static void dealWithDeath(Pet pet) {
		pet.setScore(pet.getScore()-20);
		if (!pet.getDeath()) {
			System.out.println("Your pet " + pet.getName() + " has Died");
			System.out.println("1) revive your pet (you can only do this once)");
			System.out.println("2) Continue without " + pet.getName());
			int deathOption = Integer.parseInt(scan.nextLine().trim());
			if (deathOption == 1) {
				pet.revive();
				pet.incActivityCounter();
			}else{
				pets.remove(pet);
			}
		}else{
			pet.addDeath();
			pet.kill();
			pet.incActivityCounter();
			pet.incActivityCounter();
		}
	}
	
	/**
	 * deal with sickness
	 * @param pet
	 * @return
	 */
	public static int dealWithSickness(Pet pet) {
		System.out.println("Your pet " + pet.getName() + " is sick");
		System.out.println("1) Heal your pet for $60");
		System.out.println("2) Continue without treating " + pet.getName());
		int deathOption = Integer.parseInt(scan.nextLine().trim());
		if (deathOption == 1) {
			pet.cure();
			pet.incActivityCounter();
			return 100;
		}else{
			pet.sick();
			return 0;
		}
	}
	
	/**
	 * deals with misbehavior
	 * @param pet
	 */
	public static void dealWithMisbehavier(Pet pet) { 
		System.out.println("Your pet " + pet.getName() + " has misbehaved");
		System.out.println("1) Punish your pet");
		System.out.println("2) Continue without punishing" + pet.getName());
		int deathOption = Integer.parseInt(scan.nextLine().trim());
		if (deathOption == 1) {
			pet.punish();
			pet.incActivityCounter();
		}else{
			pet.misbehaved();
		}
	}
	
	/**
	 * sorts the randomBehaviour output
	 * @param behave
	 * @param pet
	 * @return
	 */
	public static int dealWithRandomBehav(String behave, Pet pet) {
		if (behave == "Died") {
			dealWithDeath(pet);
			return 0;
		}else if (behave == "Sick") {
			return dealWithSickness(pet);
		}else if (behave == "Misbehaved") {
			dealWithMisbehavier(pet);
			return 0;
		}
		return 0;
	}
	
	/**
	 * Makes sure number of players are within the accepted range
	 * @param num
	 * @return
	 */
	public static int getPlayersNum(int num) {
		if(num > 3 || num <= 0){
			System.out.println("Number of players must be between 1 and 3");
			System.out.println("How many players would you like?(1-3)");
			getPlayersNum(Integer.parseInt(scan.nextLine().trim()));
		}
		return num;
	}
	
	/**
	 * Makes sure the number of days are within the accepted range
	 * @param days
	 * @return
	 */
	public static int getDays(int days) {
		if(days > 10 || days <= 0){
			System.out.println("Number of days must be between 1 and 10");
			System.out.println("How many days would you like to play?(1-10)");
			getDays(Integer.parseInt(scan.nextLine().trim()));
		}
		return days;
	}
	
	public static int getPetNum(int petNum) {
		if(petNum > 3 || petNum <=0){
			System.out.println("Number of pets must be between 1 and 3");
			System.out.println("please enter the number of pets you want(1-3)");
			getPetNum(Integer.parseInt(scan.nextLine().trim()));
		}
		return petNum;
	}
	
	/**
	 * offline normal game
	 */
	public static void offline(){
		/* game and players initialization */
		for(int i=1; i <playersNum+1; i++) {
			String playerName = playerName("Please enter a Player #"+i+"'s name(3-20 characters)", true);
			System.out.println("please enter the number of pets you want(1-3)");
			int petNum = getPetNum(Integer.parseInt(scan.nextLine().trim()));
			Player player = new Player(playerName, 100*petNum);
			for(int j = 1; j<petNum+1;j++) {
				String jpet = choosePet(j);
				String petName = inputName("please enter the name of your new " + jpet + " (3-20 characters)", false, player.getPets());
				player.addPet(createPet(jpet, petName));
			}	
			players.add(player);
		}
		/* main loop */
		for(currentDay=1; currentDay<days+1; currentDay++) {
			System.out.println("-------------------------");
			System.out.println("Day "+currentDay);
			System.out.println("-------------------------");
			for(int i=1; i <playersNum+1; i++) {
				Player currPlayer = players.get(i-1);
				System.out.print(currPlayer.getName()+", ");
				ArrayList<Pet> currPets = currPlayer.getPets();
				for(int k=0; k<currPets.size();k++) {
					Pet currPet = currPets.get(k);
					if (!currPet.getAlive()){
						continue;
					}
					while (currPet.getActivityCounter()<2){
						if (currentDay > 1) {
							currPlayer.changeMoney(dealWithRandomBehav(currPet.randomBehaviour(), currPet));
						}
						if (currPet.getActivityCounter()<2){
							doOption(currPet, currPlayer);
						}
					}
					currPet.addScore();
					currPet.activityCounterReset();
					currPet.endDay();
				}
				currPlayer.changeMoney(100);
			}
		}
		/* end game */
		System.out.println("\n\n\nGame Scores (average daily score)");
		setScores(playersNum);
		Collections.sort(players, Player.Comparators.SCORE);
		System.out.println("Winner: "+ players.get(0).getName());
		System.out.println("------------------------");
		for (Player player : players) {
			System.out.println(player.getName() + ": " + player.getScore());
		}
	}
	
	/**
	 * displays help information
	 */
	public static void help() {
		System.out.println("Help section \nblah blah blah");
	}
	
	
	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Store.init();
		petsInitialize();
		while(true) {
			System.out.println("Welcome to 'I dont know the name of the game yet'.");
			System.out.println("1) play offline");
			System.out.println("2) play online");
			System.out.println("3) Help section");
			System.out.println("4) Quite"); 
			int option = Integer.parseInt(scan.nextLine().trim());
			if (option == 1) {
				offline();
			}else if (option == 2) {
				GameEnvOnline.online(); 
			}else if(option == 3) {
				help();
			}else if(option == 4) {
				break;
			}
			System.out.println("1) main menu");
			System.out.println("2) quite");
			int option2 = Integer.parseInt(scan.nextLine().trim());
			if (option2 == 2) {break;}
		}
		
	}

	public static int getPlayerNum() {
		return playerNum;
	}

	public static void setPlayerNum(int playerNum) {
		GameEnv.playerNum = playerNum;
	}

	public static int getPetNum() {
		return petNum;
	}

	public static void setPetNum(int petNum) {
		GameEnv.petNum = petNum;
	}
	
	public static void setDays(int days) {
		GameEnv.days = days;
	}
	
	public static int getDays() {
		return days;
	}
	
	public static ArrayList<Player> getPlayers() {
		return players;
	}
	
	public static void setPlayers(ArrayList<Player> players) {
		GameEnv.players = players;
	}
	
	public static ArrayList<Pet> getPets() {
		return pets;
	}
	
	public static void setPets(ArrayList<Pet> pets) {
		GameEnv.pets = pets;
	}
	
	public static int getCurrentDay() {
		return currentDay;
	}
	
	public static void setCurrentDay(int day) {
		currentDay = day;
	}
	
	public static int getCurPlayer() {
		return currentPlayer;
	}
	public static void setCurPlayer(int currentPlayer) {
		GameEnv.currentPlayer = currentPlayer;
	}

	public static int getActions1() {
		return actions1;
	}

	public static void setActions1(int actions1) {
		GameEnv.actions1 = actions1;
	}

	public static int getActions2() {
		return actions2;
	}

	public static void setActions2(int actions2) {
		GameEnv.actions2 = actions2;
	}

	public static int getActions3() {
		return actions3;
	}

	public static void setActions3(int actions3) {
		GameEnv.actions3 = actions3;
	}

	public static int getCurrentPet() {
		return currentPet;
	}

	public static void setCurrentPet(int currentPet) {
		GameEnv.currentPet = currentPet;
	}

	public static Pet getPetName() {
		return petName;
	}

	public static void setPetName(Pet petName) {
		GameEnv.petName = petName;
	}

}

/*
 * sickness;
 * ArrayList<Pet>
	private int toiletMeter;
	private int hunger;
	private int tiredness;
	private int happyness;
 * for(Player obj : players){
	if(obj.getName() == playerName) {
		System.out.println("Another player is already using that name");
	}
}*/
