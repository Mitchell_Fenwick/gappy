
public class Toy {
	double duribility;
	double price;
	int funLevel;
	int excersizeLevel;
	
	public Toy(double price, double duribility, int funLevel, int excersizeLevel) {
		this.price = price;
		this.duribility = duribility;
		this.funLevel = funLevel;
		this.excersizeLevel = excersizeLevel;
	}
	
	public double getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	
	public double getDuribility() {
		return duribility;
	}
	
	public void setDuribility(double dura) {
		duribility -= dura; 
	}
	
	public int getFunLevel() {
		return funLevel;
	}
	
	public int getExcersizeLevel() {
		return excersizeLevel;
	}
}
