import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JComboBox;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StoreBox extends JDialog {

	private final JPanel mainPnl = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			StoreBox dialog = new StoreBox();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public StoreBox() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		mainPnl.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(mainPnl, BorderLayout.CENTER);
		GridBagLayout gbl_mainPnl = new GridBagLayout();
		gbl_mainPnl.columnWidths = new int[]{0, 0, 0};
		gbl_mainPnl.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_mainPnl.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_mainPnl.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		mainPnl.setLayout(gbl_mainPnl);
		
		JLabel moneyLbl = new JLabel("Money: $"+GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getMoney());
		GridBagConstraints gbc_moneyLbl = new GridBagConstraints();
		gbc_moneyLbl.insets = new Insets(0, 0, 5, 0);
		gbc_moneyLbl.gridwidth = 2;
		gbc_moneyLbl.gridx = 0;
		gbc_moneyLbl.gridy = 0;
		mainPnl.add(moneyLbl, gbc_moneyLbl);
	
		JLabel storeFoodLbl = new JLabel("Food");
		GridBagConstraints gbc_storeFoodLbl = new GridBagConstraints();
		gbc_storeFoodLbl.insets = new Insets(0, 0, 5, 5);
		gbc_storeFoodLbl.gridx = 0;
		gbc_storeFoodLbl.gridy = 1;
		mainPnl.add(storeFoodLbl, gbc_storeFoodLbl);

		JLabel storeToysLbl = new JLabel("toys");
		GridBagConstraints gbc_storeToysLbl = new GridBagConstraints();
		gbc_storeToysLbl.insets = new Insets(0, 0, 5, 0);
		gbc_storeToysLbl.gridx = 1;
		gbc_storeToysLbl.gridy = 1;
		mainPnl.add(storeToysLbl, gbc_storeToysLbl);

		JTextPane textPane = new JTextPane();
		GridBagConstraints gbc_textPane = new GridBagConstraints();
		gbc_textPane.gridheight = 4;
		gbc_textPane.insets = new Insets(0, 0, 5, 5);
		gbc_textPane.fill = GridBagConstraints.BOTH;
		gbc_textPane.gridx = 0;
		gbc_textPane.gridy = 2;
		mainPnl.add(textPane, gbc_textPane);

		JTextPane textPane2 = new JTextPane();
		GridBagConstraints gbc_textPane2 = new GridBagConstraints();
		gbc_textPane2.gridheight = 4;
		gbc_textPane2.insets = new Insets(0, 0, 5, 0);
		gbc_textPane2.fill = GridBagConstraints.BOTH;
		gbc_textPane2.gridx = 1;
		gbc_textPane2.gridy = 2;
		mainPnl.add(textPane2, gbc_textPane2);
	
		JComboBox foodCB = new JComboBox();
		String[] food = new String[Store.getFood().size()];
		for (int i=0; i <Store.getFood().size();i++){
			food[i] = Store.getFood().get(i).getClass().getSimpleName()+" $"+String.valueOf(Store.getFood().get(i).getPrice());
		}
		foodCB.setModel(new DefaultComboBoxModel(food));
		GridBagConstraints gbc_foodCB = new GridBagConstraints();
		gbc_foodCB.insets = new Insets(0, 0, 5, 5);
		gbc_foodCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_foodCB.gridx = 0;
		gbc_foodCB.gridy = 6;
		mainPnl.add(foodCB, gbc_foodCB);

		JComboBox toysCB = new JComboBox();
		String[] toys = new String[Store.getToys().size()];
		for (int i=0; i <Store.getToys().size();i++){
			toys[i] = Store.getToys().get(i).getClass().getSimpleName()+" $"+String.valueOf(Store.getToys().get(i).getPrice());
		}
		toysCB.setModel(new DefaultComboBoxModel(toys));
		GridBagConstraints gbc_toysCB = new GridBagConstraints();
		gbc_toysCB.insets = new Insets(0, 0, 5, 0);
		gbc_toysCB.fill = GridBagConstraints.HORIZONTAL;
		gbc_toysCB.gridx = 1;
		gbc_toysCB.gridy = 6;
		mainPnl.add(toysCB, gbc_toysCB);
	
		JLabel statusLbl = new JLabel("New label");
		GridBagConstraints gbc_statusLbl = new GridBagConstraints();
		gbc_statusLbl.gridwidth = 2;
		gbc_statusLbl.insets = new Insets(0, 0, 5, 5);
		gbc_statusLbl.gridx = 0;
		gbc_statusLbl.gridy = 7;
		mainPnl.add(statusLbl, gbc_statusLbl);
		
		
		JButton foodPuchaseBtn = new JButton("Purchase Food");
		foodPuchaseBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getMoney()>=Store.getFood().get(foodCB.getSelectedIndex()).getPrice()) {
					Store.buyFood(GameEnv.getPlayers().get(GameEnv.getCurPlayer()), Store.getFood().get(foodCB.getSelectedIndex()));
					statusLbl.setText("Purchase Complete");
				}else{
					statusLbl.setText("Not enough money");
				}
				statusLbl.setVisible(true);
				moneyLbl.setText("Money: $"+GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getMoney());
			}
		});
		GridBagConstraints gbc_foodPuchaseBtn = new GridBagConstraints();
		gbc_foodPuchaseBtn.insets = new Insets(0, 0, 0, 5);
		gbc_foodPuchaseBtn.gridx = 0;
		gbc_foodPuchaseBtn.gridy = 8;
		mainPnl.add(foodPuchaseBtn, gbc_foodPuchaseBtn);
		statusLbl.setVisible(false);

		JButton purchaseToyBtn = new JButton("Purchase Toy");
		purchaseToyBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getMoney()>=Store.getToys().get(toysCB.getSelectedIndex()).getPrice()) {
					Store.buyToy(GameEnv.getPlayers().get(GameEnv.getCurPlayer()), Store.getToys().get(toysCB.getSelectedIndex()));
					statusLbl.setText("Purchase Complete");
				}else{
					statusLbl.setText("Not enough money");
				}
				statusLbl.setVisible(true);
				moneyLbl.setText("Money: $"+GameEnv.getPlayers().get(GameEnv.getCurPlayer()).getMoney());
			}
		});
		GridBagConstraints gbc_purchaseToyBtn = new GridBagConstraints();
		gbc_purchaseToyBtn.gridx = 1;
		gbc_purchaseToyBtn.gridy = 8;
		mainPnl.add(purchaseToyBtn, gbc_purchaseToyBtn);
		
		
	}

}
