import java.util.*;
/**
 * @version 1.0
 * @author umutof
 *
 */
public class Pet {
	private String name;
	private int sickness;
	private int toiletMeter;
	private int hunger;
	private int tiredness;
	private int happyness;
	private double weight;
	private double score;
	private boolean sick = false;
	private boolean death = false;
	private boolean alive = true;
	private boolean naughty = false;
	private int activityCounter;
	private String favouriteToy;
	private String favouriteFood;
	private double averageWeight;
	private double tirednessMultiplier;
	private double hungerMultiplier;
	private double weightMultiplier;
	private Random randGen = new Random();
	
	/**
	 * constructor
	 * @param name
	 * @param averageWeight
	 * @param favToy
	 * @param favFood
	 * @param tiredMult
	 * @param hungMult
	 * @param weightMult
	 */
	public Pet(String name, double averageWeight, String favToy, String favFood, double tiredMult, double hungMult, double weightMult){
		this.name = name;
		this.averageWeight = averageWeight;
		weight = this.averageWeight;
		this.favouriteToy = favToy;
		this.favouriteFood = favFood;
		this.tirednessMultiplier = tiredMult;
		this.hungerMultiplier = hungMult;
		this.weightMultiplier = weightMult;
	}
	
	/**
	 * checks for spare life
	 * @return
	 */
	public boolean getDeath() {
		return death;
	}
	
	/**
	 * removes spare life
	 */
	public void addDeath() {
		death = true;
	}
	
	/**
	 * Revives pet
	 */
	public void revive() {
		addDeath();
		tiredness = 0;
		sickness = 0;
		System.out.println("Your pet has been revived");
	}
	
	/**
	 * returns string of stats
	 * @return
	 */
	public String getWeight(){
		return String.valueOf(weight);
	}
	
	public String getFavToy() {
		return favouriteToy;
	}
	
	public String getFavFood() {
		return favouriteFood;
	}
	
	public String getHunger() {
		return String.valueOf(hunger);
	}
	
	/**
	 * return name
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * return pet info
	 * @return
	 */
	public String[] getInfo() {
		return new String[] {String.valueOf(weight), String.valueOf(hunger), String.valueOf(sickness), String.valueOf(tiredness), String.valueOf(toiletMeter)};
	}
	
	/**
	 * returns activityCounter
	 * @return
	 */
	public int getActivityCounter() {
		return activityCounter;
	}
	
	/**
	 * increments activity counter
	 */
	public void incActivityCounter(){
		activityCounter++;
	}
	
	/**
	 * resets activity counter to 0
	 */
	public void activityCounterReset() {
		activityCounter = 0;
	}
	
	/**
	 * returns alive
	 * @return
	 */
	public boolean getAlive() {
		return alive;
	}
	
	/**
	 * Food is removed from players 
	 * @param player
	 * @param food
	 */
	public void feed(Food food) {
		weight += weightMultiplier * food.getWeight();
		toiletMeter += food.getToiletValue();
		if (toiletMeter > 100) {toiletMeter = 100;}
		hunger -= hungerMultiplier * food.getNutrition();
		if(hunger < 0) {hunger = 0;}
		happyness += food.getTasteValue();
		if (happyness > 100) {happyness = 100;}
	}
	
	/**
	 * Toy is played with
	 * @param player
	 * @param toy
	 */
	public void play(Toy toy) { 
		happyness += toy.getFunLevel();
		if (happyness > 100) {happyness = 100;}
		tiredness += tirednessMultiplier * toy.getExcersizeLevel();
		if (tiredness > 100) {tiredness = 100;}
		weight -= weightMultiplier * toy.getExcersizeLevel();
		if (weight < 0) {weight = 0;}
		toy.setDuribility(10);
	}
	
	public void endDay() {
		happyness -= 30;
		if (happyness < 0) {happyness = 0;}
		tiredness += 20;
		if (tiredness > 100) {tiredness = 100;}
		toiletMeter += 30;
		if (toiletMeter > 100) {toiletMeter = 100;}
		hunger+=40;
		if (hunger > 100){hunger=100;}
		sickness +=5;
		if(sickness > 100){sickness=100;}
	}
	
	/**
	 * resets pet tiredness
	 */
	public void sleep() {
		tiredness = 0;
	}
	
	/**
	 * resets pet toiletMeter
	 */
	public void toilet() {
		toiletMeter = 0;
	}
	
	public void setScore(double score){
		this.score = score;
	}
	
	public void punish() {
		happyness -= 30;
		naughty = false;
	}
	
	public void misbehaved() {
		happyness -= 20;
		naughty = true;
	}
	
	/**
	 * generates random behavior
	 * @return
	 */
	public String randomBehaviour() {
		if (sickness + tiredness + toiletMeter + hunger > 280) {
			return "Died";
		}
		
		double sick = Math.abs((weight - averageWeight)/weightMultiplier);
		sick = sick * 5 + ((sickness/10)^2) * randGen.nextInt(5);
		if(sick > 300 || this.sick) {
			return "Sick";
		}
		
		double rating = 0;
		try {
			rating = ((tiredness/10)^2)/(happyness/2);
		}catch(Exception ArithmaticException){
			rating = (tiredness/10)^2;
		}
		rating = rating + randGen.nextInt(5);
		if (rating > 6 || naughty) {
			return "Misbehaved";
		}
		return "None";
	}
	
	 /**
	  * cures pet
	  */
	public void cure() {
		sick = false;
		sickness = 0;
		happyness += 20;
		if (happyness > 100) {happyness = 100;}
	}
	
	/**
	 * makes pet sick
	 */
	public void sick() {
		sick = true;
		sickness = 100;
		happyness -= 40;
		if (happyness < 0) {happyness = 0;}
	}
	
	/**
	 * gets score
	 * @return
	 */
	public double getScore() {
		return score;
	}

	/**
	 * adds up days score
	 */
	public void addScore() {
		this.score += 100 -((tiredness+hunger+toiletMeter+sickness+100-happyness)/5);
		
	}
	
	/**
	 * kills pet
	 */
	public void kill() {
		alive = false;
	}
	
}
